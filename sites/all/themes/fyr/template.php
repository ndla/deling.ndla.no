<?php
/*
  Preprocess
*/

/*
function fyr_preprocess_html(&$vars) {
    kpr($vars);
}
*/

function fyr_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == 'search_block_form') {
    $form['search_block_form']['#attributes']['placeholder'] = t('Search for resources');
  }
}



function fyr_preprocess_page(&$vars,$hook) {

  // Font awsome CDN
  drupal_add_css('//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css','external');

  drupal_add_css('//cdn.jsdelivr.net/jquery.slick/1.3.7/slick.css','external');

  drupal_add_js('//cdn.jsdelivr.net/jquery.slick/1.3.7/slick.min.js', 'external');


  if (drupal_is_front_page()) {
    if (isset($vars['page']['content']['system_main'])) {
      unset($vars['page']['content']['system_main']);
    }
  }
}

function fyr_preprocess_node(&$vars, $hook) {
  /*
  if($vars['type'] != 'link') return;
  $sql = "INSERT INTO `field_data_field_common_courses` (`entity_type`, `bundle`, `deleted`, `entity_id`, `revision_id`, `language`, `delta`, `field_common_courses_target_id`)
          VALUES
          ('node', 'article', 0, " . $vars['nid'] . ", " . $vars['vid'] . ", 'und', 0, 7605);";

  db_query($sql);
  */
}

function fyr_preprocess_username(&$vars, $user) {
  if(!$user) {
    $user = user_load($vars['uid']);
  }
  
  $vars['name'] = !empty($user->realname) ? $user->realname : strtolower($vars['name_raw']) ;
}

function fyr_preprocess_field(&$variables) {
  $field_name = $variables['element']['#field_name'];
  $bundle = $variables['element']['#bundle'];
  if ($field_name == 'field_url' && $bundle == 'link') {
    foreach($variables['element']['#items'] as $i => $value) {
      $variables['element']['#items'][$i]['attributes']['target'] = '_blank';
    }
  }
  if ($field_name == 'field_link' && $bundle == 'teaching_programme') {
    foreach($variables['element']['#items'] as $i => $value) {
      $variables['element']['#items'][$i]['attributes']['target'] = '_blank';
    }
  }
}

function fyr_preprocess_search_api_page_result(&$variables) {
  $variables['snippet'] = strip_tags($variables['snippet']);

  if(strlen($variables['snippet']) > 250) {
    $variables['snippet'] = trim( substr( $variables['snippet'] , 0, 250) ) . '...';
  }

  $index = $variables['index'];
  $variables['id'] = $variables['result']['id'];
  $item = $variables['item'];

  $wrapper = $index->entityWrapper($item, FALSE);

  $variables['url'] = $index->datasource()->getItemUrl($item);
  $variables['title'] = $index->datasource()->getItemLabel($item);

  if (!empty($variables['result']['excerpt'])) {
    $variables['excerpt'] = $variables['result']['excerpt'];
    $text = $variables['result']['excerpt'];
  }
  else {
    $fields = $index->options['fields'];
    $fields = array_intersect_key($fields, drupal_map_assoc($index->getFulltextFields()));
    $fields = search_api_extract_fields($wrapper, $fields);
    $text = '';
    $length = 0;
    foreach ($fields as $field_name => $field) {
      if (search_api_is_list_type($field['type']) || !isset($field['value'])) {
        continue;
      }
      $val_length = drupal_strlen($field['value']);
      if ($val_length > $length) {
        $text = $field['value'];
        $length = $val_length;

        $format = NULL;
        if (($pos = strrpos($field_name, ':')) && substr($field_name, $pos + 1) == 'value') {
          $tmp = $wrapper;
          try {
            foreach (explode(':', substr($field_name, 0, $pos)) as $part) {
              if (!isset($tmp->$part)) {
                $tmp = NULL;
              }
              $tmp = $tmp->$part;
            }
          }
          catch (EntityMetadataWrapperException $e) {
            $tmp = NULL;
          }
          if ($tmp && $tmp->type() == 'text_formatted' && isset($tmp->format)) {
            $format = $tmp->format->value();
          }
        }
      }
    }
    if ($text && function_exists('text_summary')) {
      $text = text_summary($text, $format);
    }
  }

  // Check for existence. User search does not include snippets.
  $variables['snippet'] = isset($text) ? $text : '';

  // Meta information
  global $language;
  if (isset($item->language) && $item->language != $language->language && $item->language != LANGUAGE_NONE) {
    $variables['title_attributes_array']['xml:lang'] = $item->language;
    $variables['content_attributes_array']['xml:lang'] = $item->language;
  }

  $info = array();
  if (!empty($item->name)) {
    $tmp = array('name_raw' => $item->name, 'uid' => $item->uid);
    fyr_preprocess_username($tmp);
    $info['user'] = $tmp['name'];
  }
  if (!empty($item->created)) {
    $info['date'] = format_date($item->created, 'short');
  }
  // Provide separated and grouped meta information..
  $variables['info_split'] = $info;
  $variables['info'] = implode(' - ', $info);
}

function fyr_search_api_sorts_list(array $variables) {
  $options = $variables['options'];
  
  $select['select'] = array(
      '#type' => 'select', 
      '#attributes' => array(),
      '#title' => t('Sort order'), 
      '#options' => array(),
      '#required' => false,
      '#description' => '',
  );

  $select['#attached']['js'] = array(
    drupal_get_path('theme', 'fyr') . '/js/search_dropdown.js' => array(
      'type' => 'file',
    ),
  );
  
  foreach($variables['items'] as $item) {
    if(isset($item['#order_options'])) {
      $options = $item['#order_options'];
    } else {
      $options = $item['#options'];
    }
    
    $url = base_path() . current_path() . '?' . drupal_http_build_query($options['query']);
    if(!empty($_GET['sort']) && $options['query']['sort'] == $_GET['sort']) {
      $select['select']['#value'] = $url; //str_replace("&", "&amp;", $url);
    }
    $select['select']['#options'][$url] = $item['#name'];
  }

  return render($select);
}

function fyr_preprocess(&$vars, $hook) {
  $headers = drupal_get_http_header();
  if (isset($headers['status'])) {
    if($headers['status'] == '404 Not Found'){
      $key = array_search('page__404', $vars['theme_hook_suggestions']);
      if($key) {
        unset($vars['theme_hook_suggestions'][$key]);
      }
    }
  }
}

function fyr_current_search_keys($variables) {
  $params = drupal_get_query_parameters();
  $current_path = current_path();
  $current_path = explode("/", $current_path);
  array_pop($current_path);
  $current_path = implode("/", $current_path);

  $url = url($current_path, array('query' => $params, 'attributes' => array('rel' => 'nofollow')));
  
  $a_title = t('Deactivate facet !facet', array('!facet' => check_plain($variables['keys'])));
  return "<a title='" . $a_title . "' rel='nofollow' class='facet-deactivate' href='" . $url . "'>" . check_plain($variables['keys']) . "</a>";
}

function fyr_facetapi_title($variables) {
  //Remove ":" - not pretty enough for us hipsters
  return t('Filter by @title', array('@title' => drupal_strtolower($variables['title'])));
}

function fyr_facetapi_link_active($variables) {
  // Sanitizes the link text if necessary.
  $sanitize = empty($variables['options']['html']);
  $link_text = ($sanitize) ? check_plain($variables['text']) : $variables['text'];

  // Theme function variables fro accessible markup.
  // @see http://drupal.org/node/1316580
  $accessible_vars = array(
    'text' => $variables['text'],
    'active' => TRUE,
  );

  // Builds link, passes through t() which gives us the ability to change the
  // position of the widget on a per-language basis.
  $replacements = array(
    '!facetapi_deactivate_widget' => theme('facetapi_deactivate_widget', $variables),
    '!facetapi_accessible_markup' => theme('facetapi_accessible_markup', $accessible_vars),
  );
  $variables['text'] = t('!facetapi_deactivate_widget !facetapi_accessible_markup', $replacements);
  $variables['options']['html'] = TRUE;
  $a_title = t('Deactivate facet !facet', array('!facet' => $link_text));
  return "<a title='" . $a_title . "' rel='nofollow' class='facet-deactivate' href='" . url($variables['path'], array('query' => $variables['options']['query'])) . "'>" . $link_text . "</a>";
}
