(function($) {
  $(function() {
    $('.block-search-api-sorts-search-sorts select').change(function(event) {
      var $selected = $('.block-search-api-sorts-search-sorts select :selected');
      window.location = $selected.val();
    })
  })
})(jQuery)