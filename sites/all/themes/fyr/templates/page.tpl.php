<?php
//kpr(get_defined_vars());
//kpr($theme_hook_suggestions);
//template naming
//page--[CONTENT TYPE].tpl.php
?>
<?php if( theme_get_setting('mothership_poorthemers_helper') ){ ?>
<!-- page.tpl.php-->
<?php } ?>

<?php print $mothership_poorthemers_helper; ?>

<?php if($page['toolbar']): ?>
  <div class="toolbar-region">
    <?php print render($page['toolbar']); ?>
  </div>
<?php endif; ?>

<header role="banner">

  <div class="siteinfo">
    <?php if ($logo): ?>
      <figure>
      <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home">
        <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
      </a>
      </figure>
    <?php endif; ?>

    <?php if($site_name || $site_slogan ): ?>
    <hgroup>
      <?php if($site_name): ?>
        <h1><?php print $site_name; ?></h1>
      <?php endif; ?>
      <?php if ($site_slogan): ?>
        <h2><?php print $site_slogan; ?></h2>
      <?php endif; ?>
    </hgroup>
    <?php endif; ?>
  </div>

  <?php if($page['header']): ?>
    <div class="header-region">
      <?php print render($page['header']); ?>
    </div>
  <?php endif; ?>

</header>

<div class="navigation-banner">
  <nav>
  <div class="find"><?php print render($page['navigation_find']); ?></div>
    <div class="navigation-banner-seperator orange-to-white"></div>
    <div class="share"><?php print render($page['navigation_share']); ?></div>
    <div class="navigation-banner-seperator white-to-orange"></div>
  </nav>
</div>

<div class="page">

  <div role="main" id="main-content">
    <div class="content_pre">
      <?php print render($page['content_pre']); ?>
    </div>
    <?php print render($title_prefix); ?>
    <?php if ($title): ?>
      <?php 
        // Override title and make it upper case first if we are adding a new node.
        // http://bug.ndla.no/browse/RDM-2496
        if (arg(1) == 'add') $title = ucfirst(strtolower($title)); ?>
      <h1><?php print $title; ?></h1>
    <?php endif; ?>
    <?php print render($title_suffix); ?>

    <?php if ($action_links): ?>
      <ul class="action-links"><?php print render($action_links); ?></ul>
    <?php endif; ?>

    <?php if (isset($tabs['#primary'][0]) || isset($tabs['#secondary'][0])): ?>
      <nav class="tabs"><?php print render($tabs); ?></nav>
    <?php endif; ?>

    <?php if($page['highlighted'] || $messages){ ?>
      <div class="drupal-messages">
      <?php print render($page['highlighted']); ?>
      <?php print $messages; ?>
      </div>
    <?php } ?>



    <?php print render($page['content']); ?>

    <?php print render($page['content_post']); ?>

  </div><!-- /main-->

<?php if (!preg_match('/^node\/[\d]+\/edit/', current_path())): ?>
  <?php if ($page['sidebar_left']): ?>
    <div class="sidebar-left">
      <?php print render($page['sidebar_left']); ?>
    </div>
  <?php endif; ?>
  <?php if ($page['sidebar_right']): ?>
    <div class="sidebar-right">
      <?php print render($page['sidebar_right']); ?>
    </div>
  <?php endif; ?>
<?php endif; ?>

</div><!-- /page-->

<div class="content_bottom_wrapper">
  <div class="content_bottom">
    <?php print render($page['content_bottom']); ?>
  </div>
</div>

<footer role="contentinfo">
  <div class="container">
    <?php print render($page['footer']); ?>
  </div>
</footer>

