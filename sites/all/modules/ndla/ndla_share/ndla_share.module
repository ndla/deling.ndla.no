<?php

/**
 * Implements hook_menu().
 */
function ndla_share_menu() {
  $menu = array();
  $menu['node/%common_course/top-authors'] = array(
    'title' => t('Top Authors'),
    'page callback' => 'ndla_share_top_authors_page',
    'page arguments' => array(1),
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );
  $menu['top-authors'] = array(
    'title' => t('Top Authors'),
    'page callback' => 'ndla_share_top_authors_global_page',
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );
  $menu['shared_content/widget'] = array(
    'title' => t('My Shared Content'),
    'page callback' => 'ndla_share_display_widget',
    'access callback' => 'ndla_share_widget_access',
    'type' => MENU_CALLBACK,
    'file' => 'ndla_share.widget.inc'
  );
  return $menu;
}

function ndla_share_widget_access() {
  global $user;
  if (!$user->uid && !empty($_GET['rau'])) {
    seriaauth_roamauth($_GET['rau']);
  }
  return ($user->uid == TRUE);
}

/**
 * Implements hook_menu_alter().
 */
function ndla_share_menu_alter(&$items) {
  $items['node/%node']['access callback'] = 'ndla_share_node_view_access';
  $items['node/%node/edit']['access callback'] = 'ndla_share_co_editor_access';
  
  $items['h5peditor/%/libraries']['access callback'] = 'ndla_share_co_editor_access';
  $items['h5peditor/%/libraries']['access arguments'] = array('update', 1);
  $items['h5peditor/%/libraries/%/%/%']['access callback'] = 'ndla_share_co_editor_access';
  $items['h5peditor/%/libraries/%/%/%']['access arguments'] = array('update', 1);
  
  $items['h5peditor/%/files']['access callback'] = 'ndla_share_co_editor_access';
  $items['h5peditor/%/files']['access arguments'] = array('update', 1);
}

function ndla_share_node_access($node, $op, $account = NULL) {
  if(arg(0) != 'h5peditor' && ($op == 'view' || $op == 'create')) {
    $view = ndla_share_node_view_access('view', $node, $account, TRUE);
    $update = ndla_share_co_editor_access($op, $node, $account, TRUE);
    if($op == 'update' && $update) {
      return NODE_ACCESS_ALLOW;
    }
    if($op == 'view' && ($view || $update)) {
     return NODE_ACCESS_ALLOW;
   }
 }

 return NODE_ACCESS_IGNORE;
}

function ndla_share_node_view_access($op, $node, $account = NULL, $skip_node_access = FALSE) {
  if($op == 'view') {
    if(!empty($node->nid) && ndla_share_access($node->nid)) {
      return TRUE;
    }
  }
  $back = ndla_share_co_editor_access($op, $node, $account, $skip_node_access);
  return $back;
}

function ndla_share_access($nid) {
  $has_access = FALSE;
  $parameters = drupal_get_query_parameters();
  if(empty($parameters['key'])) return false;

  if($parameters['key'] == md5($nid . variable_get('ndla_share_salt', ''))) {
    $has_access = TRUE;
  }

  return $has_access;
}

/**
 * Implements hook_views_plugins().
 */
function ndla_share_views_plugins() {
  $plugins = array(
    'access' => array(
      'test' => array(
        'title' => t('NDLA Share Access'),
        'help' => t('Check if the correct key param is set'),
        'handler' => 'ndla_share_access_plugin',
        'path' => drupal_get_path('module', 'ndla_share'),
      ),
    ),
  );
  return $plugins;
}

/**
 * Implements hook_block_info().
 */
function ndla_share_block_info() {
  $blocks['share-shortcuts'] = array(
    'info' => t('Share shortcuts'),
  );
  $blocks['share-secret-link'] = array(
    'info' => t('Share secret link'),
  );
  $blocks['top-authors'] = array(
    'info' => t('Top authors'),
  );
  $blocks['top-authors-global'] = array(
    'info' => t('Top authors - global'),
  );
  return $blocks;
}

/**
 * Implements hook_block_configure().
 */
function ndla_share_block_configure($delta) {
  switch ($delta) {
    case 'share-shortcuts':
      $types = node_type_get_types();
      foreach($types as $name => $type) {
        $options[$name] = $type->name;
      }
      $blacklisted = variable_get('ndla_share_blacklist', array());
      $form['share'] = array(
        '#type' => 'fieldset',
        '#title' => t('Share'),
        '#description' => t('Remove bundles from this blocks share function.'),
        'blacklist' => array(
          '#title' => t('Blacklist'),
          '#type' => 'checkboxes',
          '#options' => $options,
          '#default_value' => $blacklisted,
        ),
      );
      return $form;
      break;
    case 'share-secret-link':
      $form['salt'] = array(
        '#type' => 'fieldset',
        '#title' => t('Share'),
        '#description' => t('Set security for secret share link.'),
        'salt' => array(
          '#title' => t('Salt'),
          '#type' => 'textfield',
          '#default_value' => variable_get('ndla_share_salt', ''),
          '#description' => t('Any text string.'),
        ),
      );
      return $form;
      break;
    case 'top-authors-global':
      $form['blacklist'] = array(
        '#type' => 'textarea',
        '#title' => t('Blacklist'),
        '#description' => t('Specify blacklisted users by using their uids. Enter one path per line.'),
        '#default_value' => implode("\n", variable_get('ndla_share_top_authors_global_blacklist', array())),
      );
      return $form;
      break;
    case 'top-authors':
      $form['blacklist'] = array(
        '#type' => 'textarea',
        '#title' => t('Blacklist'),
        '#description' => t('Specify blacklisted users by using their uids. Enter one path per line.'),
        '#default_value' => implode("\n", variable_get('ndla_share_top_authors_blacklist', array())),
      );
      return $form;
      break;
  }
}

/**
 * Implements hook_block_save().
 */
function ndla_share_block_save($delta, $edit = array()) {
  switch ($delta) {
    case 'share-shortcuts':
      variable_set('ndla_share_blacklist', array_filter($edit['blacklist']));
      break;
    case 'share-secret-link':
      variable_set('ndla_share_salt', $edit['salt']);
      break;
    case 'top-authors':
      $blacklist = explode("\n", $edit['blacklist']);
      variable_set('ndla_share_top_authors_blacklist', $blacklist);
      break;
    case 'top-authors-global':
      $blacklist = explode("\n", $edit['blacklist']);
      variable_set('ndla_share_top_authors_global_blacklist', $blacklist);
      break;
  }
}

/**
 * Implements hook_block_view().
 */
function ndla_share_block_view($delta = '') {
  global $base_url, $base_path;
  module_load_include('inc', 'ndla_share', 'ndla_share.blocks');
  switch ($delta) {
    case 'share-shortcuts':
      return ndla_share_shortcuts_block();
    case 'share-secret-link':
      return ndla_share_secret_link_block();
    case 'top-authors':
      return ndla_share_top_authors_block();
    case 'top-authors-global':
      return ndla_share_top_authors_global_block();
  }
}

function ndla_share_form($form, &$form_state) {
  $types = node_type_get_types();
  $options = array(t('Choose a type to share'));
  $blacklisted = variable_get('ndla_share_blacklist', array());
  foreach($types as $name => $type) {
    if(!array_key_exists($name, $blacklisted) && node_access('create', $name)) {
      $title = NULL;
      if(function_exists('i18n_node_type_name')) {
        $title = i18n_node_type_name($name, $type->name);
      }
      else {
        $title = t($type->name);
      }

      $name_id = str_replace("_", "-", $name);
      $options[$name_id] = $title;
    }
  }
  if(count($options) <= 1) {
    return;
  }
  $form['bundle'] = array(
    '#type' => 'select',
    '#options' => $options,
    '#attributes' => array(
      'onchange' => 'document.getElementById("ndla-share-form").submit();'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Share'),
  );
  return $form;
}

function ndla_share_form_submit($form, $form_state) {
  $bundle = $form_state['input']['bundle'];
  if(!empty($bundle)) {
    drupal_goto('node/add/' . $bundle);
  }
}

/**
 * Implements hook_form_alter().
 */
function ndla_share_form_alter(&$form, &$form_state, $form_id) {
  global $user;

  /* Return if not a node form */
  if(empty($form['#node']->type) || $form_id != $form['#node']->type . "_node_form") {
    return;
  }

  if(ndla_auth_authenticated()) {
    $county = ndla_auth_county();
    if(empty($county)) {
      $form['county'] = array(
        '#title' => t('Users county'),
        '#type' => 'select',
        '#required' => TRUE,
        '#options' => array(
          '' => t('Not selected'),
          'akershus' => t('Akershus'),
          'aust-agder' => t('Aust-Agder'),
          'buskerud' => t('Buskerud'),
          'finnmark' => t('Finnmark'),
          'hedmark' => t('Hedmark'),
          'hordaland' => t('Hordaland'),
          'moreogromsdal' => t('More og Romsdal'),
          'nord-trondelag' => t('Nord-Trondelag'),
          'nordland' => t('Nordland'),
          'oslo' => t('Oslo'),
          'rogaland' => t('Rogaland'),
          'sognogfjordane' => t('Sogn og Fjordane'),
          'sor-trondelag' => t('Sor-Trondelag'),
          'telemark' => t('Telemark'),
          'troms' => t('Troms'),
          'vest-agder' => t('Vest-Agder'),
          'vestfold' => t('Vestfold'),
          'oppland' => t('Oppland'),
          'ostfold' => t('Ostfold'),
        ),
        '#weight' => -100,
      );
      $form['#submit'][] = 'ndla_share_fylke_submit';
    }
  }

  if(!empty($form['#node']->field_co_editors[LANGUAGE_NONE])) {
    foreach($form['#node']->field_co_editors[LANGUAGE_NONE] as $editor) {
      if($editor['target_id'] == $user->uid) {
        $fields = array(
          'field_co_editors',
        );
        foreach($fields as $field) {
          $form[$field]['#type'] = 'hidden';
        }
      }
    }
  }
}

function ndla_share_fylke_submit($form, $form_state) {
  if(!empty($form_state['values']['county'])) {
    ndla_auth_county($form_state['values']['county']);
  }
}

function ndla_share_views_access($account) {
  $nid = arg(1);
  $has_access_via_key = ndla_share_access($nid);

  if($has_access_via_key) return $has_access_via_key;

  $node = node_load($nid);
  return node_access('view', $node, $account);
}


/**
 * Implements hook_init().
 */
function ndla_share_init() {
  global $user;
  if(!ndla_auth_authenticated()) {
    return;
  }
  $account = user_load($user->uid);
  $county = ndla_auth_county();
  if(empty($account->field_county) || $account->field_county[LANGUAGE_NONE][0]['value'] != $county) {
    $account->field_county[LANGUAGE_NONE][0]['value'] = $county;
    user_save($account);
  }
}

/**
 * Loader function that performs as an access calback for thr Competence Aims tab
 * @param int $nid
 */
function common_course_load($nid) {
  $node = node_load($nid);
  if(empty($node) || $node->type != 'common_course') {
    return FALSE;
  }
  return $node;
}

function ndla_share_top_authors_global_page() {
  $blacklist = array_filter(variable_get('ndla_share_top_authors_global_blacklist', array()));
  $blacklist[] = 0;
  global $base_path;
  $items = array();
  //Too old to use the new DB API.
  $result = db_query("SELECT d.field_display_name_value, u.name, u.uid, count(u.uid) AS num_shares FROM {users} u
            INNER JOIN {node} n ON n.uid = u.uid
            LEFT JOIN {field_data_field_display_name} d ON d.entity_id = n.uid
            WHERE u.name <> '' AND u.uid NOT IN(:blacklisted)
            GROUP BY u.uid ORDER BY num_shares DESC", array(':blacklisted' => $blacklist));

  $count = 1;
  while($row = $result->fetchObject()) {
    $username = $row->name;
    if(!empty($row->field_display_name_value)) {
      $username = $row->field_display_name_value;
    }

    if(strlen($username) > 20) {
      $username = trim(substr($username, 0, 19)) . "...";
    }

    $class = $count;
    if($class > 3) {
      $class = 'other';
    }
    $items[] = array(
      'data' => "<a href='{$base_path}search?f[0]=author:{$row->uid}'>$username</a><br>" . t('!resources shared', array('!resources' => $row->num_shares)),
      'class' => array('position_' . $class)
    );
    $count++;
  }
  
  $items = array_chunk($items, ceil(sizeof($items) / 2));
  $html = '<div class="ndla-share-top-authors top-authors">';
  for($i = 0; $i < 2; $i++) {
    $html .= theme('item_list', array('items' => $items[$i]));
  }
  $html .= '</div>';
  return $html;
}

function ndla_share_top_authors_page($node) {
  global $base_path;
  $blacklist = variable_get('ndla_share_top_authors_blacklist', array());
  $blacklist[] = 0;
  $query = db_select('field_data_field_common_courses', 'fcc');
  $query->leftjoin('node', 'n', 'fcc.entity_id = n.nid');
  $query->leftjoin('users', 'u', 'n.uid = u.uid');
  $query->leftjoin('field_data_field_display_name', 'd', 'n.uid = d.entity_id');
  $query->addExpression('COUNT(u.uid)', 'count');
  $data = $query
    ->fields('u', array('name', 'uid'))
    ->fields('d', array('field_display_name_value'))
    ->condition('fcc.field_common_courses_target_id', $node->nid)
    ->condition('u.uid', $blacklist, 'NOT IN')
    ->condition('n.status', 1)
    ->groupBy('u.uid')
    ->orderBy('count', 'DESC')
    ->orderBy('u.name', 'ASC')
    ->execute()
    ->fetchAll();
    $items = array();
    foreach($data as $key => $user) {
      $username = '';
      if(!empty($user->field_display_name_value)) {
        $username = $user->field_display_name_value;
      } else {
        $username = $user->name;
      }
      $cquery = db_select('field_data_field_courses', 'fc');
      $cquery->leftjoin('node', 'n', 'fc.entity_id = n.nid');
      $cquery->leftjoin('field_data_field_common_courses', 'fcc', 'n.nid = fcc.entity_id');
      $cquery->leftjoin('users', 'u', 'n.uid = u.uid');
      $cquery->leftjoin('field_data_field_display_name', 'd', 'n.uid = d.entity_id');
      $courses = $cquery
        ->fields('u', array('name', 'uid'))
        ->condition('u.uid', $user->uid)
        ->condition('n.status', 1)
        ->condition('fcc.field_common_courses_target_id', $node->nid)
        ->groupBy('fc.field_courses_target_id')
        ->execute()
        ->rowCount();
      $class = ($key < 3) ? 'position_' . ($key + 1) : 'position_other';
      $items[] = array(
        'data' => "<a href='{$base_path}search?f[0]=author:{$user->uid}&f[1]=field_common_courses:{$node->nid}'>$username</a><br>" . t('!resources shared resources within !areas areas', array('!resources' => $user->count, '!areas' => $courses)),
        'class' => array($class)
      );
    }
    $items = array_chunk($items, ceil(sizeof($items) / 2));
    $html = '<div class="ndla-share-top-authors top-authors">';
    for($i = 0; $i < 2; $i++) {
      $html .= theme('item_list', array('items' => $items[$i]));
    }
    $html .= '</div>';
    return $html;
}

function ndla_share_facetapi_filters() {
  return array(
    'real_name' => array(
      'handler' => array(
        'label' => t('Convert usernames to Realname from Seria Auth'),
        'class' => 'FacetapiRealnameConvert',
      ),
    ),
  );
}

class FacetapiRealnameConvert extends FacetapiFilter {
  public function execute(array $build) {

    $uids = array();
    foreach($build as $data)
      $uids[] = $data['#indexed_value'];

    if(!empty($uids)) {
      $results = db_select('field_data_field_display_name', 'f')
                     ->fields('f', array('entity_id','field_display_name_value'))
                     ->condition('f.entity_id', $uids, 'IN')
                     ->condition('bundle', 'user')
                     ->execute()
                     ->fetchAll();

      $usermap = array();
      foreach($results as $row)
        $usermap[$row->entity_id] = $row->field_display_name_value; 

      foreach($build as &$data) {
        if(!empty($usermap[$data['#indexed_value']])) {
          $data['#markup'] = $usermap[$data['#indexed_value']];
        }
      }
    }
    return $build;
  }
}

function ndla_share_co_editor_access($op = 'update', $node, $account = NULL, $skip_node_access = FALSE) {
  //Special for h5peditor
  if(empty($node->nid) && $node == 0) {
    if(arg(0) == 'h5peditor' && module_exists('h5peditor')) {
      return h5peditor_access($node);
    }
  }
  
  if(is_numeric($node)) {
    $node = node_load($node);
  }
  
  global $user;

  if(!empty($node->field_co_editors[LANGUAGE_NONE])) {
    foreach($node->field_co_editors[LANGUAGE_NONE] as $editor) {
      if($editor['target_id'] == $user->uid) {
        return TRUE;
      }
    }
  }
  if(!$skip_node_access) {
    return node_access($op, $node);
  }
}