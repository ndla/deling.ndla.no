<?php

function ndla_share_widget_get_nodes() {
  global $user, $base_url, $base_path;
  $result = db_select('node', 'n')
    ->fields('n', array('title', 'status', 'nid', 'type', 'changed', 'language'))
    ->condition('n.uid', $user->uid)
    ->orderBy('n.changed', 'DESC')
    ->execute();
  $rows = array();
  while($data = $result->fetchAssoc()) {
    $rows[] = array(
      'status' => empty($data['status']) ? t('Draft') : t('Published'),
      'url' => $base_url . $base_path . 'node/' . $data['nid'],
      'title' => $data['title'],
      'type' => $data['type'],
      'changed' => $data['changed'],
      'language' => $data['language'],
    );
  }
  return $rows;
}

function ndla_share_display_widget() {
  global $language;

  $types = node_type_get_types();
  /* Get view */
  $full = (!empty($_GET['view']) && $_GET['view'] == 'full');

  /* Get nodes */
  $nodes = ndla_share_widget_get_nodes();

  /* Build output json */
  $out = array(
    'type' => 'table',
    'data' => array(
      'headers' => array(
        array(
          'text' => t('Title'),
          'sortable' => TRUE
        ),
        array(
          'text' => t('Status'),
          'sortable' => TRUE
        ),
      ),
      'rows' => array(
      ),
      'more' => (!$full) ? t('Full view') : FALSE,
    ),
  );
  if($full) {
    $out['data']['headers'][] = array(
      'text' => t('Content type'),
      'sortable' => TRUE,
    );
    $out['data']['headers'][] = array(
      'text' => t('Updated'),
      'sortable' => TRUE,
    );
  }
  
  foreach($nodes as $node) {
    $row = array(
      array(
        'text' => mb_substr($node['title'], 0, 30),
        'link' => $node['url'],
      ),
      array(
        'text' => $node['status'],
      ),
    );
    if(!empty($node['language']) && !in_array($node['language'], array('und', $language->language))) {
      $row[0]['lang'] = $node['language'];
    }
    if($full) {
      $row[] = array(
        'text' => $types[$node['type']]->name,
      );
      $row[] = array(
        'text' => date('d.m.Y', $node['changed']),
        'sort' => $node['changed'],
      );
    }
    $out['data']['rows'][] = $row;
  }

  /* Print result */
  drupal_add_http_header('Content-Type', 'application/json; charset=utf-8');
  echo $_GET['callback'] . '(' . json_encode($out) . ')';
  die();
}