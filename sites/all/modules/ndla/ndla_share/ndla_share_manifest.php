[general]
name = sc_<?php print(substr(preg_replace('/[\W]/', '', $_SERVER['HTTP_HOST']), 0 , 16)); ?>

title = Shared Content

[widgets]
widget.name[] = msc_<?php print(substr(preg_replace('/[\W]/', '', $_SERVER['HTTP_HOST']), 0 , 16)); ?>

widget.title[] = My Shared Content - <?php print($_SERVER['HTTP_HOST']); ?>

widget.url[] = /nb/shared_content/widget
widget.display_type[] = half
widget.deletable[] = 1
