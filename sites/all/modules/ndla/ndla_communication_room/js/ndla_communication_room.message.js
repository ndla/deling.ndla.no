(function ($) {
  Drupal.behaviors.ndla_communication_room = {
    attach: function (context, settings) {
      
      $('#edit-selected option:not(:selected)').remove();
      jQuery('#edit-selected option:selected').each(function() {
        var value = $(this).val();
        $('#edit-to-users option[value=' + value + ']').remove();
      });
      Drupal.behaviors.ndla_communication_room.filter_users();
      
      /* Select group */
      $('#edit-to-groups', context).click(function() {
        Drupal.behaviors.ndla_communication_room.filter_users();
        Drupal.behaviors.ndla_communication_room.mark_users();
      });
      /* Select users */
      $('#edit-select-button', context).click(function(e) {
        e.preventDefault();
        Drupal.behaviors.ndla_communication_room.select_users();
      });
      /* Unselect users */
      $('#edit-unselect-button', context).click(function(e) {
        e.preventDefault();
        Drupal.behaviors.ndla_communication_room.unselect_users();
        Drupal.behaviors.ndla_communication_room.filter_users();
      });
    },
    select_users: function() {
      $('#edit-to-users option:selected').appendTo('#edit-selected');
    },
    unselect_users: function() {
      $('#edit-selected option:selected').appendTo('#edit-to-users');
    },
    filter_users: function () {
      $('#edit-to-users option').appendTo('#edit-hidden-users');
      $('#edit-to-groups option:selected').each(function() {
        var gid = $(this).val();
        $('#edit-hidden-users option').each(function() {
          var value = $(this).val()
          if(value.match(RegExp('gid' + gid))) {
            $(this).appendTo('#edit-to-users');
          }
        });
      });
    },
    mark_users: function () {
      $('#edit-to-users option').each(function() {
        $(this).attr('selected', 'selected');
      })
    },
  };
})(jQuery);