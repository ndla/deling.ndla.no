<div class='ndla-socialmedia'>
  <div id='google-plusone-content'>
  	<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>
  	<div class="g-plusone" data-size="standard" data-count="true" data-href="<?php echo htmlspecialchars($likeUrl); ?>"></div>
  </div>
  <div id='twitter-like-content'>
  	<a href="https://twitter.com/share" class="twitter-share-button" data-via="ndla_no" data-related="ndla_no" data-hashtags="ndla">Tweet</a>
  	<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
  </div>
  <div id='facebook-like-content'>
  	<div id="fb-root"></div>
  	<script>(function(d, s, id) {
  			var js, fjs = d.getElementsByTagName(s)[0];
  			if (d.getElementById(id)) return;
  			js = d.createElement(s); js.id = id;
  			js.src = "//connect.facebook.net/nb_NO/all.js#xfbml=1&appId=" + <?php echo json_encode($fbappid); ?>;
  			fjs.parentNode.insertBefore(js, fjs);
  	}(document, 'script', 'facebook-jssdk'));</script>

  	<div class="fb-like" data-href="<?php echo htmlspecialchars($likeUrl); ?>" data-width="450" data-height="31" data-colorscheme="light" data-layout="standard" data-action="like" data-show-faces="true" data-send="false"></div>
  </div>
</div>