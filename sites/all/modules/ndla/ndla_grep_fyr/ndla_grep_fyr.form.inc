<?php

/**
 * Implements hook_form_alter().
 */
function ndla_grep_fyr_form_alter(&$form, &$form_state, $form_id) {
  /* Return if not a node form */
  if(empty($form['#node']->type) || $form_id != $form['#node']->type . "_node_form") {
    return;
  }
  /* Return if content type is not enabled */
  $content_types = array_filter(variable_get('ndla_grep_content_types', array()));
  if(!in_array($form['#node']->type, $content_types)) {
    return;
  }

  $aims = array(
    'courses' => array(
      'data' => array(),
      'aims' => array(),
    ),
    'common_courses' => array(
      'data' => array(),
      'aims' => array(),
    ),
  );
  if(!empty($form['#node']->nid)) {
    $aims = NdlaGrepFyrClient::load_aims($form['#node']);
  }

  foreach(array('courses' => 'Courses', 'common_courses' => 'Common courses') as $key => $name) {
    $field = 'field_' . $key;
    if(!empty($form[$field])) {
      $form[$field][LANGUAGE_NONE]['#description'] = t('Select competence aims this resource is relevant for');
      $form[$field]['ndla_grep_' . $key] = array(
        '#prefix' => '<div id="ndla_grep_' . $key . '_aim_select">',
        '#suffix' => '</div>',
        '#tree' => TRUE,
        '#weight' => 10,
        'button' => array(
          '#type' => 'submit',
          '#value' => t('Select ' . strtolower($name)),
          '#ajax' => array(
            'callback' => 'ndla_grep_fyr_' . $key . '_aim_select',
            'wrapper' => 'ndla_grep_' . $key . '_aim_select',
            'effect' => 'slide',
          ),
          '#limit_validation_errors' => array(),
          '#submit' => array(),
        ),
        'hidden' => array(
          '#type' => 'hidden',
          '#default_value' => '',
        ),
        'markup' => array(
          '#type' => 'markup',
          '#markup' => '',
        ),
      );
      $selected = '';
      if(!empty($aims[$key])) {
        $selected = theme('ndla_grep_fyr_aim_selected_container', array('items' => $aims[$key]));
      }
      $form[$field]['ndla_grep_' . $key . '_selected'] = array(
        '#tree' => TRUE,
        '#weight' => 11,
        'data' => array(
          '#type' => 'hidden',
          '#default_value' => json_encode($aims[$key]),
        ),
        'hidden' => array(
          '#type' => 'hidden',
          '#default_value' => $selected,
        ),
        'markup' => array(
          '#prefix' => '<div id="ndla_grep_' . $key . '_aim_selected">',
          '#suffix' => '</div>',
          '#type' => 'markup',
          '#markup' => '',
        ),
      );
    }
  }

  /* Make common courses required */
  $form['field_common_courses']['und']['#title'] = t($form['field_common_courses']['und']['#title']) . ' <span class="form-required" title="This field is required.">*</span>';
  $form['#validate'][] = 'ndla_grep_fyr_validate';

  $form['#after_build'][] = 'ndla_grep_fyr_after_build';
  array_unshift($form['actions']['submit']['#submit'], 'ndla_grep_fyr_submit');
}

function ndla_grep_fyr_after_build($form, &$form_state) {
  drupal_add_css(drupal_get_path('module', 'ndla_grep_fyr') . '/css/ndla_grep_fyr.form.css', 'header');
  drupal_add_js(drupal_get_path('module', 'ndla_grep_fyr') .'/js/ndla_grep_fyr.form.js', 'file');
  drupal_add_css('//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css','external');
  foreach(array('courses', 'common_courses') as $name) {
    $field = 'field_' . $name;
    if(!empty($form[$field]['ndla_grep_' . $name]['hidden']['#value'])) {
      $form[$field]['ndla_grep_' . $name]['markup']['#markup'] = $form[$field]['ndla_grep_' . $name]['hidden']['#value'];
      unset($form[$field]['ndla_grep_' . $name]['button']);
    }
    if(!empty($form[$field]['ndla_grep_' . $name . '_selected']['hidden']['#value'])) {
      $form[$field]['ndla_grep_' . $name . '_selected']['markup']['#markup'] = $form[$field]['ndla_grep_' . $name . '_selected']['hidden']['#value'];
    }
  }
  return $form;
}

function ndla_grep_fyr_validate($form, &$form_state) {
  $limit = 1;
  foreach(array(/*'courses' => 'courses',*/ 'common_courses' => 'common courses') as $key => $name) {
    $stored = json_decode($form_state['values']['field_' . $key]['ndla_grep_' . $key . '_selected']['data'], TRUE);
    $aims = ndla_grep_fyr_recursive_get_aims($stored);
    if(sizeof($aims) < $limit) {
      form_set_error('field_' . $key, t('You need to choose at least !limit competance aims for !type.', array('!type' => $name, '!limit' => $limit)));
    }
  }
}

function ndla_grep_fyr_submit($form, &$form_state) {
  $new_aims = array();
  foreach(array('courses', 'common_courses') as $name) {
    if(!empty($form_state['values']['field_' . $name]['ndla_grep_' . $name . '_selected']['data'])) {
      $data = json_decode($form_state['values']['field_' . $name]['ndla_grep_' . $name . '_selected']['data']);
      $courses = array();
      foreach($data as $key => $value) {
        $courses[] = $key;
      }
      $form_state['values']['field_' . $name][LANGUAGE_NONE] = array();
      if(!empty($courses)) {
        $data = db_select('field_data_field_grep_id', 'i')
          ->fields('i', array('entity_id'))
          ->condition('i.field_grep_id_value', $courses, 'IN')
          ->execute()
          ->fetchAll();
        foreach($data as $course) {
          $form_state['values']['field_' . $name][LANGUAGE_NONE][] = array(
            'target_id' => $course->entity_id,
          );
        }
      }
      if(empty($form_state['values']['field_' . $name][LANGUAGE_NONE])) {
        $form_state['values']['field_' . $name][LANGUAGE_NONE][] = array(
          'target_id' => '',
        );
      }
    }
    $form_state['node']->{'ndla_grep_' . $name} = $form_state['values']['field_' . $name]['ndla_grep_' . $name . '_selected']['data'];
  }
}

function ndla_grep_fyr_selector_response($form, $menu, $type) {
  $form['markup']['#markup'] = theme('ndla_grep_fyr_aim_select_container', array('menu' => $menu, 'tree' => array(), 'type' => $type));
  $form['hidden']['#value'] = $form['markup']['#markup'];
  unset($form['button']);
  return $form;
}

function ndla_grep_fyr_courses_aim_select($form, &$form_state) {
  $ids = ndla_grep_fyr_get_all_course_ids();
  $courses = NdlaGrepFyrClient::get_courses($ids);
  $menu = theme('ndla_grep_fyr_aim_select_list', array('items' => $courses, 'callback' => url('ndla_grep/ajax/courses/course')));
  return ndla_grep_fyr_selector_response($form['field_courses']['ndla_grep_courses'], $menu, 'courses');
}

function ndla_grep_fyr_ajax_courses_course() {
  $parameters = drupal_get_query_parameters();
  $grep_id = $parameters['tree'][0]['#id'];
  $levels = ndla_grep_fyr_get_levels($grep_id);
  $items = NdlaGrepFyrClient::get_course($parameters['endpoint'], $levels);
  foreach($items as $key => $item) {
    if(!empty($item['children'])) {
      $tree = $parameters['tree'];
      $tree[] = array(
        '#name' => $item['name'],
        '#id' => $item['id'],
      );
      $items[$key]['children'] = theme('ndla_grep_fyr_aim_select_list', array('items' => $item['children'], 'callback' => url('ndla_grep/ajax/courses/structure'), 'hidden' => TRUE, 'tree' => $tree));
    }
  }
  echo theme('ndla_grep_fyr_aim_select_list', array('items' => $items, 'tree' => $parameters['tree']));
}

function ndla_grep_fyr_ajax_courses_structure() {
  $parameters = drupal_get_query_parameters();
  $items = NdlaGrepFyrClient::get_course_structure($parameters['endpoint']);
  echo theme('ndla_grep_fyr_aim_select_list', array('items' => $items, 'target' => 'right', 'callback' => url('ndla_grep/ajax/courses/aims'), 'tree' => $parameters['tree']));
}

function ndla_grep_fyr_ajax_courses_aims() {
  $parameters = drupal_get_query_parameters();
  $data = NdlaGrepFyrClient::get_aims($parameters['endpoint']);
  $tree = $parameters['tree'];
  $stored = json_decode($parameters['stored'], TRUE);
  $selected = array();
  if(!empty($stored[$tree[0]['#id']][$tree[1]['#id']][$tree[2]['#id']][$tree[3]['#id']][$tree[4]['#id']])) {
    $selected = $stored[$tree[0]['#id']][$tree[1]['#id']][$tree[2]['#id']][$tree[3]['#id']][$tree[4]['#id']];
  }
  echo theme('ndla_grep_fyr_aim_select_boxes', array('data' => $data, 'tree' => $tree, 'selected' => $selected));
}

function ndla_grep_fyr_common_courses_aim_select($form, &$form_state) {
  $ids = ndla_grep_fyr_get_all_course_ids();
  $common_courses = NdlaGrepFyrClient::get_common_courses($ids);
  $menu = theme('ndla_grep_fyr_aim_select_list', array('items' => $common_courses, 'callback' => url('ndla_grep/ajax/common/curriculum')));
  return ndla_grep_fyr_selector_response($form['field_common_courses']['ndla_grep_common_courses'], $menu, 'common_courses');
}

function ndla_grep_fyr_ajax_common_curriculum() {
  $parameters = drupal_get_query_parameters();
  $grep_id = $parameters['tree'][0]['#id'];
  $levels = ndla_grep_fyr_get_levels($grep_id);
  $sets = ndla_grep_fyr_get_competence_aims_sets($grep_id);
  $items = NdlaGrepFyrClient::get_curriculum($parameters['endpoint'], $levels, $sets);
  echo theme('ndla_grep_fyr_aim_select_list', array('items' => $items, 'target' => 'right', 'callback' => url('ndla_grep/ajax/common/sets'), 'tree' => $parameters['tree']));
}

function ndla_grep_fyr_ajax_common_sets() {
  $parameters = drupal_get_query_parameters();
  $tree = $parameters['tree'];
  $stored = json_decode($parameters['stored'], TRUE);
  $selected = array();
  if(!empty($stored[$tree[0]['#id']][$tree[1]['#id']][$tree[2]['#id']][$tree[3]['#id']][$tree[4]['#id']])) {
    $selected = $stored[$tree[0]['#id']][$tree[1]['#id']][$tree[2]['#id']][$tree[3]['#id']][$tree[4]['#id']];
  }
  $data = NdlaGrepFyrClient::get_aim_set($parameters['endpoint']);
  echo theme('ndla_grep_fyr_aim_select_boxes', array('data' => $data, 'tree' => $tree, 'selected' => $selected));
}

function ndla_grep_fyr_ajax_store() {
  $parameters = drupal_get_query_parameters();
  $tree = $parameters['tree'];
  $stored = json_decode($parameters['stored'], TRUE);
  $bool = ndla_grep_fyr_recursive_is_set($tree, $stored);
  if(!$bool) {
    $stored = ndla_grep_fyr_recursive_set($tree, $stored);
  } else {
    $stored = ndla_grep_fyr_recursive_delete($tree, $stored);
  }
  $html = '';
  if(!empty($stored)) {
    $html = theme('ndla_grep_fyr_aim_selected_container', array('items' => $stored));
  }
  echo json_encode(array('json' => json_encode($stored), 'html' => $html));
}

function ndla_grep_fyr_recursive_is_set($list, $array) {
  if(empty($list)) {
    return TRUE;
  }
  $item = array_shift($list);
  $id = $item['#id'];
  if(!array_key_exists($item['#id'], $array)) {
    return FALSE;
  }
  return ndla_grep_fyr_recursive_is_set($list, $array[$id]);
}

function ndla_grep_fyr_recursive_set($list, $array) {
  $item = array_shift($list);
  $id = $item['#id'];
  if(!array_key_exists($id, $array)) {
    $array[$id] = $item;
  }
  if(!empty($list)) {
    $array[$id] = ndla_grep_fyr_recursive_set($list, $array[$id]);
  }
  return $array;
}

function ndla_grep_fyr_recursive_delete($list, $array) {
  $item = array_shift($list);
  $id = $item['#id'];
  if(empty($list)) {
    unset($array[$id]);
    return $array;
  } else {
    $array[$id] = ndla_grep_fyr_recursive_delete($list, $array[$id]);
    if(sizeof(element_children($array[$id])) == 0) {
      unset($array[$id]);
    }
  }
  return $array;
}

function ndla_grep_fyr_recursive_get_aims($items) {
  $aims = array();
  $has_children = FALSE;
  foreach($items as $key => $value) {
    if(!in_array($key, array('#name', '#id'))) {
      $aims = array_merge($aims, ndla_grep_fyr_recursive_get_aims($value));
      $has_children = TRUE;
    }
  }
  if(!$has_children && !empty($items['#id'])) {
    $aims[] = $items['#id'];
  }
  return $aims;
}

function ndla_grep_fyr_get_all_course_ids() {
  $query = db_select('field_data_field_grep_id', 'i');
  $query->leftjoin('node', 'n', 'i.entity_id = n.nid');
  $data = $query
    ->fields('i', array('field_grep_id_value'))
    ->condition('n.status', '1')
    ->execute()
    ->fetchAll();
  $ids = array();
  foreach($data as $level) {
    $ids[] = $level->field_grep_id_value;
  }
  return $ids;
}

function ndla_grep_fyr_get_levels($grep_id) {
  $query = db_select('field_data_field_grep_id', 'i');
  $query->leftjoin('field_data_field_grep_levels', 'l', 'i.entity_id = l.entity_id');
  $data = $query
    ->fields('l', array('field_grep_levels_value'))
    ->condition('i.field_grep_id_value', $grep_id)
    ->execute()
    ->fetchAll();
  $levels = array();
  foreach($data as $level) {
    if(!empty($level->field_grep_levels_value)) {
      $levels[] = $level->field_grep_levels_value;
    }
  }
  return $levels;
}

function ndla_grep_fyr_get_competence_aims_sets($grep_id) {
  $query = db_select('field_data_field_grep_id', 'i');
  $query->leftjoin('field_data_field_grep_competence_aim_sets', 's', 'i.entity_id = s.entity_id');
  $data = $query
    ->fields('s', array('field_grep_competence_aim_sets_value'))
    ->condition('i.field_grep_id_value', $grep_id)
    ->execute()
    ->fetchAll();
  foreach($data as $field) {
    $text = $field->field_grep_competence_aim_sets_value;
    if(!empty($text)) {
      return explode("\n", $text);
    }
  }
  return array();
}

function ndla_grep_fyr_ajax_remove() {
  $parameters = drupal_get_query_parameters();
  $id_list = $parameters['id_list'];
  $stored = json_decode($parameters['stored'], TRUE);
  $stored = ndla_grep_fyr_recursive_remove($id_list, $stored);
  $html = '';
  if(!empty($stored)) {
    $html = theme('ndla_grep_fyr_aim_selected_container', array('items' => $stored));
  }
  echo json_encode(array('json' => json_encode($stored), 'html' => $html));
}

function ndla_grep_fyr_recursive_remove($list, $array) {
  $item = array_shift($list);
  $id = $item;
  if(empty($list)) {
    unset($array[$id]);
    return $array;
  } else {
    $array[$id] = ndla_grep_fyr_recursive_remove($list, $array[$id]);
    if(sizeof(element_children($array[$id])) == 0) {
      unset($array[$id]);
    }
  }
  return $array;
}
