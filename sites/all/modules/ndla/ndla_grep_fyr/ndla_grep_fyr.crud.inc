<?php

/**
 * Implements hook_node_insert().
 */
function ndla_grep_fyr_node_insert($node) {
  $content_types = array_filter(variable_get('ndla_grep_content_types', array()));
  if(in_array($node->type, $content_types)) {
    $result = NdlaGrepFyrClient::save_resource($node);
    if(!$result) {
      drupal_set_message(t('Could not save resource at !host', array('!host' => variable_get('ndla_grep_host', ''))), 'error');
    } else {
      ndla_grep_fyr_node_update_aims($node);
    }
  }
}

/**
 * Implements hook_node_update().
 */
function ndla_grep_fyr_node_update($node) {
  $content_types = array_filter(variable_get('ndla_grep_content_types', array()));
  if(in_array($node->type, $content_types) && (isset($node->ndla_grep_courses) || isset($node->ndla_grep_common_courses))) {
    ndla_grep_fyr_node_update_aims($node);
  }
}

function ndla_grep_fyr_node_update_aims($node) {
  NdlaGrepFyrClient::delete_aims($node);
  $new_aims = array();
  /* Courses */
  if(!empty($node->ndla_grep_courses)) {
    $data = json_decode($node->ndla_grep_courses, TRUE);
    foreach($data as $course) {
      foreach(array_intersect_key($course, array_flip(element_children($course))) as $level) {
        foreach(array_intersect_key($level, array_flip(element_children($level))) as $course_structure) {
          foreach(array_intersect_key($course_structure, array_flip(element_children($course_structure))) as $curriculum_set) {
            foreach(array_intersect_key($curriculum_set, array_flip(element_children($curriculum_set))) as $curriculum) {
              foreach(array_intersect_key($curriculum, array_flip(element_children($curriculum))) as $competence_aim_set) {
                foreach(array_intersect_key($competence_aim_set, array_flip(element_children($competence_aim_set))) as $competence_aim) {
                  $new_aims[] = array(
                    'id' => $competence_aim['#id'],
                    'curriculum_set_id' => $curriculum_set['#id'],
                    'level' => $level['#id'],
                    'course_id' => $course['#id'],
                    'course_type' => 'EDUCATIONAL_PROGRAM',
                    'curriculum_id' => $curriculum['#id'],
                    'competence_aim_set_id' => $competence_aim_set['#id'],
                  );
                }
              }
            }
          }
        }
      }
    }
  }
  /* Common courses */
  if(!empty($node->ndla_grep_common_courses)) {
    $data = json_decode($node->ndla_grep_common_courses, TRUE);
    foreach($data as $course) {
      foreach(array_intersect_key($course, array_flip(element_children($course))) as $curriculum_set) {
        foreach(array_intersect_key($curriculum_set, array_flip(element_children($curriculum_set))) as $curriculum) {
          foreach(array_intersect_key($curriculum, array_flip(element_children($curriculum))) as $level) {
            foreach(array_intersect_key($level, array_flip(element_children($level))) as $competence_aim_set) {
              foreach(array_intersect_key($competence_aim_set, array_flip(element_children($competence_aim_set))) as $main_groups) {
                foreach(array_intersect_key($main_groups, array_flip(element_children($main_groups))) as $competence_aim) {
                  $new_aims[] = array(
                    'id' => $competence_aim['#id'],
                    'curriculum_set_id' => $curriculum_set['#id'],
                    'level' => $level['#id'],
                    'course_id' => $course['#id'],
                    'course_type' => 'CURRICULUM',
                    'curriculum_id' => $curriculum['#id'],
                    'competence_aim_set_id' => $main_groups['#id'],
                  );
                }
              }
            }
          }
        }
      }
    }
  }
  /* Save */
  NdlaGrepFyrClient::save_aims($node, $new_aims);
}

/**
 * Implements hook_node_delete().
 */
function ndla_grep_fyr_node_delete($node) {
  NdlaGrepFyrClient::delete_aims($node);
}