<?php print $billboard; ?>
<h3><?php echo t('Vocational courses') ?></h3>
<div id='courses-list'>
  <?php foreach($lists as $courses): ?>
  <ul>
    <?php foreach($courses as $course): ?>
      <li class='course'>
        <?php if(!empty($course['nid'])): ?>
            <a href="<?php print $GLOBALS['base_path']; ?>search?f[0]=field_courses%3A<?php print $course['nid']; ?>&f[1]=field_common_courses%3A<?php print $nid; ?>"><?php print $course['title']; ?></a>
          <?php else: ?>
            <a href="<?php print $GLOBALS['base_path']; ?>search?f[0]=field_common_courses%3A<?php print $nid; ?>&f[1]=field_courses:!"><?php print $course['title']; ?></a>
          <?php endif; ?>
      </li>
    <?php endforeach ?>
  </ul>
  <?php endforeach ?>
</div>