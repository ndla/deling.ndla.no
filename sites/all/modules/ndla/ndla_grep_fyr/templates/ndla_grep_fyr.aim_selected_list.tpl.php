<?php
if(empty($items['#name'])) {
  foreach(array_intersect_key($items, array_flip(element_children($items))) as $item) {
    $temp_list = $id_list;
    $temp_list[] = $item['#id'];
    print theme('ndla_grep_fyr_aim_selected_list', array('items' => $item, 'id_list' => $temp_list));
  }
} else { ?>
<ul>
  <li>
    <span>
      <?php
        print $items['#name'];
        $children = element_children($items);
        if(empty($children)):
      ?>
        <i class="fa fa-times aim-list-remove" data-tree='<?php echo(json_encode($id_list)); ?>'></i>
      <?php endif; ?>
    </span>
    <?php
      foreach(array_intersect_key($items, array_flip(element_children($items))) as $item) {
        $temp_list = $id_list;
        $temp_list[] = $item['#id'];
        print theme('ndla_grep_fyr_aim_selected_list', array('items' => $item, 'id_list' => $temp_list));
      }
    ?>
  </li>
</ul>
<?php } ?>