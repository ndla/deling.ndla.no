<?php
  foreach($data as $list):
    $json_tree = $tree;
    $json_tree[] = array(
      '#name' => $list['name'],
      '#id' => $list['id'],
    );
?>
  <b><?php print $list['name'] ?></b>
  <ul>
    <?php foreach($list['aims'] as $aim):
        $json_aim_tree = $json_tree;
        $json_aim_tree[] = array(
          '#name' => $aim['name'],
          '#id' => $aim['id'],
        );
        $json_aim_tree = json_encode($json_aim_tree);
      ?>
      <li>
        <input <?php if(!empty($selected[$list['id']][$aim['id']])) print 'checked'; ?> type='checkbox' class='aim-list-checkbox' id='<?php print $aim['id'] ?>' data-tree='<?php print $json_aim_tree ?>'>
        <label for='<?php print $aim['id'] ?>'><?php print $aim['name'] ?></label>
      </li>
    <?php endforeach ?>
  </ul>
<?php endforeach ?>