(function ($) {
  Drupal.behaviors.exampleModule = {
    attach: function (context, settings) {
      $('#ndla_grep_search_0 li').click(function(){
        $('#ndla_grep_search_0 li').removeClass('active');
        $(this).addClass('active');
        var typeclass = $(this).attr('class').match(/^[^\s]*/);
        $('#ndla_grep_search_1 li').hide();
        $('#ndla_grep_search_1 li.' + typeclass).show();
        for(i=2;i<5;i++){
          $('#ndla_grep_search_' + i).html('');
        }
      });
    }
  };

  ndla_grep_search_update = function(element) {
    $element = $(element);
    var number = $element.parents('div').first().attr('id').match(/[\d]+/);
    $('#ndla_grep_search_' + number + ' li').removeClass('active');
    $element.addClass('active');
    number++;
    var callback = $(element).attr('callback');
    var endpoint = $(element).attr('endpoint');
    var grep_id = $(element).attr('grep_id');
    num = number;
    $('#ndla_grep_search_' + num).html('<i class="fa fa-spinner fa-spin"></i>');
    while(num < 4) {
      num++;
      $('#ndla_grep_search_' + num).html('');
    }
    $.ajax({
      url: callback,
      data: { endpoint : endpoint, grep_id : grep_id },
      success: function(data) {
        $('#ndla_grep_search_' + number).html(data);
      }
    });
  }
})(jQuery);