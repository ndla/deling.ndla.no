<?php

/**
 * Implements hook_block_info().
 */
function ndla_grep_fyr_block_info() {
  $blocks['ndla_grep_fyr_all_courses'] = array(
    'info' => t('NDLA Grep: All courses'),
    'cache' => DRUPAL_CACHE_GLOBAL,
  );
  $blocks['ndla_grep_fyr_courses'] = array(
    'info' => t('NDLA Grep: Courses'),
    'cache' => DRUPAL_CACHE_PER_PAGE,
  );
  $blocks['ndla_grep_fyr_competence_aims'] = array(
    'info' => t('NDLA Grep: Competence aims'),
    'cache' => DRUPAL_CACHE_PER_PAGE,
  );
  $blocks['ndla_grep_fyr_aims_link'] = array(
    'info' => t('NDLA Grep: Competence aims link'),
    'cache' => DRUPAL_CACHE_PER_PAGE,
  );
  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function ndla_grep_fyr_block_view($delta = '') {
  $block = array();
  switch($delta) {
    case 'ndla_grep_fyr_all_courses':
      $block = ndla_grep_fyr_all_courses_block();
      break;
    case 'ndla_grep_fyr_courses':
      $block = ndla_grep_fyr_courses_block();
      break;
    case 'ndla_grep_fyr_competence_aims':
      $block = ndla_grep_fyr_competence_aims_block();
      break;
    case 'ndla_grep_fyr_aims_link':
      $block = ndla_grep_fyr_competence_aims_link();
      break;
  }
  return $block;
}

function ndla_grep_fyr_all_courses_block() {

  /* Get all common courses */
  $common = array();
  $nodes = node_load_multiple(array(), array('type' => 'common_course'));
  foreach($nodes as $node) {
    $common[$node->title] = array(
      'title' => $node->title,
      'nid' => $node->nid,
      'icon' => theme('image_style', array('path' => $node->field_logo_image[LANGUAGE_NONE][0]['uri'], 'style_name' => 'course_logo')),
    );
  }
  ksort($common);

  /* Get all courses */
  $courses = array();
  $nodes = node_load_multiple(array(), array('type' => 'course'));
  foreach($nodes as $node) {
    $courses[$node->title] = array(
      'title' => $node->title,
      'nid' => $node->nid,
    );
  }
  ksort($courses);
  array_unshift($courses, array('title' => t('All vocational courses'), 'nid' => ''));

  /* Render output */
  $content = theme('ndla_grep_fyr_all_courses_block', array('courses' => $courses, 'common' => $common));
  return array('subject' => '', 'content' => $content);
}

function ndla_grep_fyr_courses_block() {
  /* Get current common course */
  $billboard = '';
  $path = current_path();
  if(preg_match('/^node\/([\d]+)/', $path, $matches)) {
    $nid = $matches[1];
    $node = node_load($nid);
    if(!empty($node->field_billboard_image)) {
      $billboard = theme('image_style', array('path' => $node->field_billboard_image[LANGUAGE_NONE][0]['uri'], 'style_name' => 'course_billboard'));
    }
  }

  /* Get all courses */
  $courses = array();
  $nodes = node_load_multiple(array(), array('type' => 'course'));
  foreach($nodes as $node) {
    $courses[$node->title] = array(
      'title' => $node->title,
      'nid' => $node->nid,
    );
  }
  ksort($courses);
  $courses[] = array('title' => t('All vocational courses'), 'nid' => '');

  $lists = array_chunk($courses, ceil(sizeof($courses)/2));

  /* Render output */
  $content = theme('ndla_grep_fyr_courses_block', array('lists' => $lists, 'billboard' => $billboard, 'nid' => $nid));
  return array('subject' => '', 'content' => $content);
}

function ndla_grep_fyr_competence_aims_block() {
  global $base_path;
  $path = current_path();
  if(!preg_match('/^node\/([\d]+)$/', $path, $matches)) {
    return;
  }
  $nid = $matches[1];
  $node = node_load($nid);

  /* Return if content type is not enabled */
  $content_types = array_filter(variable_get('ndla_grep_content_types', array()));
  if(!in_array($node->type, $content_types)) {
    return;
  }

  global $user;
  $account = user_load($user->uid);
  if(!node_access('view', $node, $account)) {
    return;
  }

  if($node) {
    $aims = NdlaGrepFyrClient::load_aims($node);
  }

  $common_courses_list = array();
  $course_list = array();

  /* Common courses */
  foreach($aims['common_courses'] as $course) {
    foreach(array_intersect_key($course, array_flip(element_children($course))) as $curriculum_set) {
      foreach(array_intersect_key($curriculum_set, array_flip(element_children($curriculum_set))) as $curriculum) {
        $competence_aim_set_list = array();
        foreach(array_intersect_key($curriculum, array_flip(element_children($curriculum))) as $level) {

          foreach(array_intersect_key($level, array_flip(element_children($level))) as $competence_aim_set) {
            $aim_list = array();
            foreach(array_intersect_key($competence_aim_set, array_flip(element_children($competence_aim_set))) as $main_groups) {
              foreach(array_intersect_key($main_groups, array_flip(element_children($main_groups))) as $competence_aim) {
                $aim_list[] = array('class' => array('aims'), 'data' => '<a href="' . $base_path . 'search?f[0]=ndla_grep_competence_aims_id:' . urlencode($competence_aim['#id']) . '" class="aim">' . $competence_aim['#name'] . '</a>');
              }
            }
            $competence_aim_set_list[] = array(
              'data' => theme('html_tag', array('element' => array('#tag' => 'span', '#attributes' => array('class' => 'subtitle'), '#value' => $competence_aim_set['#name']))),
                'children' => $aim_list,
            );
          }
        }
        $common_courses_list[] = array(
          'data' => theme('html_tag', array('element' => array('#tag' => 'span', '#attributes' => array('class' => 'title'), '#value' => $curriculum['#name']))),
          'children' => $competence_aim_set_list,
        ); 
      }
    }
  }
  /* Courses */
  foreach($aims['courses'] as $course) {
    $curriculum_list = array();
    foreach(array_intersect_key($course, array_flip(element_children($course))) as $level) {
      foreach(array_intersect_key($level, array_flip(element_children($level))) as $course_structure) {
        foreach(array_intersect_key($course_structure, array_flip(element_children($course_structure))) as $curriculum_set) {
          foreach(array_intersect_key($curriculum_set, array_flip(element_children($curriculum_set))) as $curriculum) {
            $aim_list = array();
            foreach(array_intersect_key($curriculum, array_flip(element_children($curriculum))) as $competence_aim_set) {
              foreach(array_intersect_key($competence_aim_set, array_flip(element_children($competence_aim_set))) as $competence_aim) {
                $aim_list[] = array('class'=> array('aims'),'data' => '<a href="' . $base_path . 'search?f[0]=ndla_grep_competence_aims_id:' . urlencode($competence_aim['#id']) . '" class="aim">' . $competence_aim['#name'] . '</a>');
              }
            }
            $curriculum_list[] = array(
              'data' => theme('html_tag', array('element' => array('#tag' => 'span', '#attributes' => array('class' => 'subtitle'), '#value' => $curriculum['#name']))),
              'children' => $aim_list,
            );
          }
        }
      }
    }
    $course_list[] = array(
      'data' => theme('html_tag', array('element' => array('#tag' => 'span', '#attributes' => array('class' => 'title'), '#value' => $course['#name']))),
      'children' => $curriculum_list,
    );
  }

  /* Render output */
  drupal_add_js(drupal_get_path('module', 'ndla_grep_fyr') . '/js/ndla_grep_fyr_block.js', 'file');
  $course_list_content = theme('item_list', array('attributes' => array('class' => 'course_list'),'items' => $course_list));
  $common_courses_list_content = theme('item_list', array('attributes' => array('class' => 'common_courses_list'),'items' => $common_courses_list));

  $wrapper_html = "<div class='list_wrapper'>" . $common_courses_list_content . $course_list_content  ."</div>";
  $bottom_html = "
    <div class='bottom_fader'>
      <div class='transparent'></div>
      <a class='show-more' href='#'>" . t('Show more') . "</a>
    </div>";
  return array('subject' => '', 'content' => $wrapper_html . $bottom_html);
}

function ndla_grep_fyr_competence_aims_link() {
  $block['subject'] = '';
  $attributes = array('class' => array('competence_aims', 'link'));
  $block['content'] = l(t('Competence aims'), 'ndla_grep/search', array('attributes' => $attributes));
  return $block;
}