<?php

function ndla_creative_commons_settings_form() {
  $form = array();
  $licenses = ndla_creative_commons_licenses();
  foreach($licenses as $key => $data) {
    $licenses[$key] = $data['title'];
  }
  $form['ndla_creative_commons_licenses'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Enabled licenses'),
    '#options' => $licenses,
    '#default_value' => variable_get('ndla_creative_commons_licenses', array()),
  );
  return system_settings_form($form);
}