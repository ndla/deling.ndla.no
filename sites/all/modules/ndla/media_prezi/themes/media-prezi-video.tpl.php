<?php

/**
 * @file media_prezi/themes/media-prezi-video.tpl.php
 *
 * Template file for theme('media_prezi_video').
 *
 * Variables available:
 *  $uri - The media uri for the Prezi video (e.g., prezi://v/xsy7x8c9).
 *  $video_id - The unique identifier of the Prezi video (e.g., xsy7x8c9).
 *  $id - The file entity ID (fid).
 *  $url - The full url including query options for the Prezi iframe.
 *  $options - An array containing the Media Prezi formatter options.
 *  $api_id_attribute - An id attribute if the Javascript API is enabled;
 *  otherwise NULL.
 *  $width - The width value set in Media: Prezi file display options.
 *  $height - The height value set in Media: Prezi file display options.
 *  $title - The Media: Prezi file's title.
 *  $alternative_content - Text to display for browsers that don't support
 *  iframes.
 *
 */

?>
<div class="<?php print $classes; ?> media-prezi-<?php print $id; ?>">
  <iframe class="media-prezi-player" <?php print $api_id_attribute; ?>width="<?php print $width; ?>" height="<?php print $height; ?>" title="<?php print $title; ?>" src="<?php print $url; ?>" frameborder="0" allowfullscreen><?php print $alternative_content; ?></iframe>
</div>
