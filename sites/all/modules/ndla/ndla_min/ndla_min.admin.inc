<?php

function ndla_min_settings() {
  $form = array();
  $form['ndla_min_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Min backend server url'),
    '#default_value' => variable_get('ndla_min_url', 'http://min.ndla.no'),
    '#required' => TRUE,
  );
  $form['ndla_min_my_subjects'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable my subjects'),
    '#default_value' => variable_get('ndla_min_my_subjects', TRUE),
  );
  return system_settings_form($form);
}