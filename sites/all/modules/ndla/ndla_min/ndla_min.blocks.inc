<?php

/**
 * Implements hook_block_info().
 */
function ndla_min_block_info() {
  $blocks['ndla_min_toolbar'] = array(
    'info' => t('NDLA Min: Toolbar'),
  );
  $blocks['ndla_min_favorite'] = array(
    'info' => t('NDLA Min: Favorite'),
  );
  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function ndla_min_block_view($delta = '') {
  if(!ndla_auth_authenticated()) {
    return;
  }
  $block = array();
  switch ($delta) {
    case 'ndla_min_favorite':
      $block = ndla_min_favorite_block();
      break;
    case 'ndla_min_toolbar':
      $block = ndla_min_toolbar_block();
      break;
  }
  return $block;
}

function ndla_min_favorite_block() {
  $path = current_path();
  if(preg_match('/^node\/([\d]+)/', $path, $matches)) {
    $nid = $matches[1];
    $data = NdlaMinClient::getInstance()->get('page', 'favorite');
    $marked = '';
    if($data != FALSE) {
      $marked = ' marked';
    }
    $content = "<span class='fa-stack fa-lg nid-$nid$marked'><i class='fa fa-star fa-stack-1x'></i><i class='fa fa-star-o fa-stack-1x'></i></span>";
    drupal_add_css(drupal_get_path('module', 'ndla_min') . '/css/ndla_min.favorite_block.css', 'header');
    drupal_add_js(drupal_get_path('module', 'ndla_min') .'/js/ndla_min.favorite_block.js', 'file');
    return array('subject' => '', 'content' => $content);
  }
}

function ndla_min_toolbar_block() {
  $list = array();
  drupal_alter('ndla_min_selected_subjects', $list);
  if(empty($list)) {
    $list[] = t('None');
  }
  $data = NdlaMinClient::getInstance()->getAll();
  drupal_add_js(drupal_get_path('module', 'ndla_min') .'/js/ndla_min.toolbar_block.js', 'file');
  drupal_add_css(drupal_get_path('module', 'ndla_min') . '/css/ndla_min.toolbar_block.css');
  return array(
    'subject' => '',
    'content' => theme('min_toolbar', array(
        'subjects' => $list,
        'name' => ndla_auth_display_name(),
        'image' => ndla_auth_profile_picture(),
        /**
         * The url should be fetched from ndla_auth_logout_url()
         * but the function returns a leading slash
         */
        'logout_url' => url('seriaauth/logout'),
        'settings_url' => variable_get('ndla_user_settings_url', 'http://min.ndla.no/usersettings/usersettings'),
        'dashboard_url' => variable_get('ndla_min_url', 'http://min.ndla.no'),
      )
    ),
  );
}