<div id="header">
  <a href="http://ndla.no">
    <img src="/sites/all/modules/ndla/ndla_min/images/ndla.png" alt="" id="logo" class="logo">
  </a>
	<nav id="navigation">
	  <ul>
      <li class="submenu">
        <a href="<?php print $dashboard_url ?>">
          <i class="fa fa-archive"></i> <?php print t('My archive'); ?>
        </a>
      </li>
      <?php if(variable_get('ndla_min_my_subjects', TRUE)): ?>
      <li class="submenu subjects">
        <span>
          <i class="fa fa-cogs"></i><?php print t('My subjects') ?>
        </span>
        <ul class="dropdown">
        <?php foreach($subjects as $key => $title): ?>
          <li>
            <a><?php print $title; ?></a>
          </li>
        <?php endforeach; ?>
        <li>
        <a class="button" href="<?php print $GLOBALS['base_path']; ?>ndla_min/my_subjects"><?php print t('Choose subjects'); ?></a>
        </li>
        </ul>
      </li>
      <?php endif; ?>
      <li class="submenu">        
        <span>
          <div id="user-icon">
            <?php if($image != false): ?>
              <img src="<?php print $image ?>&width=25&height=25&method=scaledowncrop">
            <?php else: ?>
              <i class="fa fa-user"></i>
            <?php endif; ?>
          </div>
          <?php print t('Welcome') ?> <?php print $name ?>
        </span>
		    <ul class="dropdown">
					<li>
            <a href="<?php print $settings_url ?>">
              <i class="fa fa-user"></i> <?php print t('My user') ?>
            </a>
          </li>
					<li>
            <a href="<?php print $logout_url ?>">
              <i class="fa fa-sign-out"></i><?php print t('Log out') ?>
            </a>
          </li>
				</ul>
			</li>
    </ul>
	</nav>
</div>