<?php

class NdlaMinClient {
  
  private static $client;
  private $data;
  
  private function __construct() {
    $this->data = $this->call(array('msservices_op' => 'check_in'));
  }

  private static function call($fields = array()) {
    global $language, $base_url;
    $default_fields = array(
      'msservices_roamauth' => seriaauth_get_auth_value('roamauth'),
      'site' => $base_url,
      'page' => current_path(),
      'title' => drupal_get_title(),
      'language' => $language->language,
    );
    $fields += $default_fields;
    $query = http_build_query($fields);

    $url = variable_get('ndla_min_url', 'http://min.ndla.no') . '/sites/all/modules/ndla_msservices/ndla_msservices.php';
    $result = drupal_http_request($url, array('method' => 'POST', 'data' => $query));
    if(!empty($result->data)) {
      return json_decode($result->data);
    }
    return FALSE;
  }

  public static function getInstance() {
    if(is_null(self::$client)) {
       self::$client = new NdlaMinClient();
    }
    return self::$client;
  }

  public function getAll() {
    return $this->data;
  }

  public function set($scope, $type, $data, $node = FALSE) {
    global $base_path;
    if($node) {
      $page = 'node/' . $node->nid;
      $title = $node->title;
    } else {
      $page = $base_path;
      $title = drupal_get_title();
    }
    $fields = array(
      'page' => $page,
      'title' => $title,
      'scope' => $scope,
      'namespace' => $type,
      'data' => json_encode($data),
      'msservices_op' => 'set',
    );
    $result = $this->call($fields);
    return $result;
  }

  public function delete($scope, $type, $node = FALSE) {
    global $base_path;
    if($node) {
      $page = 'node/' . $node->nid;
      $title = $node->title;
    } else {
      $page = $base_path;
      $title = drupal_get_title();
    }
    $fields = array(
      'page' => $page,
      'title' => $title,
      'scope' => $scope,
      'namespace' => $type,
      'msservices_op' => 'delete',
    );
    $result = $this->call($fields);
    return $result;
  }

  public function get($scope, $type) {
    $data = $this->data;
    if(!empty($data->$scope->$type)) {
      return $data->$scope->$type;
    }
    return FALSE;
  }

}