<div id='canvas'>
  <?php foreach($items as $name => $data): ?>
    <div <?php if($name == t('All')) print "id='hub'"; ?> class='item'>
      <h2><?php print t($name) ?></h2>
      <a href="<?=$data['link']?>"><?= $data['count']; ?> <?php print t('resources'); ?></a>
    </div>
  <?php endforeach; ?>
</div>