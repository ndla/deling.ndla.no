(function ($) {
  Drupal.behaviors.ndla_wiris = {
    attach: function (context, settings) {
      $('#edit-parse').click(function(event) {
        $('#ndla-wiris-throbber').addClass('loading');
        event.preventDefault();
        $('#edit-ndla-wiris-tags').html('');
        $.ajax({
          url: Drupal.settings.basePath + 'admin/config/content/wiris/parse',
          success: function(data) {
            $('#edit-ndla-wiris-tags').html(data);
            $('#ndla-wiris-throbber').removeClass('loading');
          }
        });
      });
    },
  };
})(jQuery);