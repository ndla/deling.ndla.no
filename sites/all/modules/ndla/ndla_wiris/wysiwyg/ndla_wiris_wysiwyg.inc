<?php

/**
 * Implementation of hook_wysiwyg_plugin().
 */
function ndla_wiris_ndla_wiris_wysiwyg_plugin() {
  $plugins['ndla_wiris_wysiwyg'] = array(
    'title' => t('Wiris Formula Editor'),
    'vendor url' => 'http://www.wiris.com/',
    'icon file' => 'wiris-formula.gif',
    'icon title' => 'Formula editor',
    'settings' => array(
      'dialog' => array(
        'url' => url('wiris/editor'),
        'width' => 500,
        'height' => 400,
        'scroll' => 'no',
        'resizable' => 'yes',
      ),
    ),
    'extended_valid_elements' => array(
      "img[id|class|style]", 
      "math[xmlns]",
    ),
  );
  return $plugins;
}
