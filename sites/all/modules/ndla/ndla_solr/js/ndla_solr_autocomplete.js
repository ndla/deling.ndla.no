Drupal.jsAC.prototype.hidePopup = function (keycode) {
  // Select item if the right key or mousebutton was pressed
  if (this.selected && ((keycode && keycode != 46 && keycode != 8 && keycode != 27) || !keycode)) {
    this.input.value = this.selected.autocompleteValue;
    var form_id = $('#' + this.input.id).parents('form').attr('id');
    if(typeof form_id != 'undefined') {
      if(form_id.indexOf('search') != -1 && form_id != 'ndla-editor-portal-search-form') {
        $('input[type=submit]', $('#' + form_id)).click();
        $(this.popup).fadeOut('fast');
        return;
      }
    }
  }

  // Hide popup
  var popup = this.popup;
  if (popup) {
    this.popup = null;
    $(popup).fadeOut('fast', function() { $(popup).remove(); });
  }
  this.selected = false;
  
  if(keycode == 13) {
    var form_id = $('#' + this.input.id).parents('form').attr('id');
    if(typeof form_id != 'undefined') {
      if(form_id.indexOf('search') != -1 && form_id != 'ndla-editor-portal-search-form') {
        $('input[type=submit]', $('#' + form_id)).click();
      }
    }
  }
};
