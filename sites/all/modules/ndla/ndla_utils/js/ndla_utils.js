(function ($) {
  var $win = $(window);

  /**
   * Check if an element is currently in view.
   *
   * @param {jQuery} $el
   *   Jquery DOM object
   * @return {Boolean}
   */
  function inView($el) {
    var elBounds = {};
    var viewport = {};

    // Find the edges of the users viewport
    viewport.top    = $win.scrollTop();
    viewport.left   = $win.scrollLeft();
    viewport.right  = viewport.left + $win.width();
    viewport.bottom = viewport.top + $win.height();

    // Get the position of the elements edges
    elBounds        = $el.offset();
    elBounds.right  = elBounds.left + $el.outerWidth();
    elBounds.bottom = elBounds.top + $el.outerHeight();

    return !(
      viewport.right  < elBounds.left  ||
      viewport.left   > elBounds.right ||
      viewport.bottom < elBounds.top   ||
      viewport.top    > elBounds.bottom
    );
  }

  Drupal.behaviors.ndlaUtilsForm = {
    attach: function (context, settings) {
      var $prefixElement = $('#js-edit-actions-prefix');
      var $pinElement    = $('#edit-actions');
      var pinClass       = 'is-pinned';

      $win.scroll(function () {
        // We use an element that is prefixed right before the element we
        // want to manipulate to check for position, instead of caching the
        // main elements old position.
        var elInView = inView($prefixElement);

        // If not in view we add a class to the element we want viewable
        // in the viewport.
        if (elInView === true) {
          $pinElement.removeClass(pinClass);
        } else {
          $pinElement.addClass(pinClass);
        }
      });
    }
  };
}(jQuery));
