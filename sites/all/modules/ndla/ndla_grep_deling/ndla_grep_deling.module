<?php

module_load_include('inc', 'ndla_grep_deling', 'ndla_grep_deling.form');
module_load_include('inc', 'ndla_grep_deling', 'ndla_grep_deling.crud');
module_load_include('inc', 'ndla_grep_deling', 'ndla_grep_deling.blocks');
module_load_include('inc', 'ndla_grep_deling', 'ndla_grep_deling.solr');
module_load_include('php', 'ndla_grep_deling', 'NdlaGrepLanguages');
module_load_include('php', 'ndla_grep_deling', 'NdlaGrepDelingClient');
module_load_include('php', 'ndla_grep_deling', 'NdlaGrepSolrClient');

/**
 * Implements hook_flush_caches().
 */
function ndla_grep_deling_flush_caches() {
  return array('cache_ndla_grep');
}

/**
 * Implements hook_permission().
 */
function ndla_grep_deling_permission() {
  return array(
    'administer ndla_grep' => array(
      'title' => t('Administer NDLA Grep'),
      'description' => t('Perform administration tasks for the NDLA GREP module.'),
    ),
  );
}

/**
 * Implements hook_menu()
 */
function ndla_grep_deling_menu() {
  $items = array();
  $items['admin/config/services/ndla_grep'] = array(
    'title' => 'NDLA Grep',
    'description' => t('Configure NDLA Grep'),
    'page callback' => 'drupal_get_form',
    'page arguments' =>  array('ndla_grep_deling_settings_form'),
    'access arguments' => array('administer ndla_grep'),
    'file' => 'ndla_grep_deling.admin.inc',
  );
  $items['ndla_grep/ajax/search/%'] = array(
    'title' => 'NDLA Grep: Search',
    'page callback' => 'ndla_grep_deling_ajax_search',
    'page arguments' => array(3),
    'access arguments' => array('access content'),
    'file' => 'ndla_grep_deling.form.inc',
  );
  $items['ndla_grep/ajax/curriculum'] = array(
    'title' => 'NDLA Grep: Curriculum',
    'page callback' => 'ndla_grep_deling_ajax_curriculum',
    'access arguments' => array('access content'),
    'file' => 'ndla_grep_deling.form.inc',
  );
  $items['ndla_grep/ajax/set'] = array(
    'title' => 'NDLA Grep: Competence aim sets',
    'page callback' => 'ndla_grep_deling_ajax_set',
    'access arguments' => array('access content'),
    'file' => 'ndla_grep_deling.form.inc',
  );
  $items['ndla_grep/ajax/store'] = array(
    'title' => 'NDLA Grep: Store',
    'page callback' => 'ndla_grep_deling_ajax_store',
    'access arguments' => array('access content'),
    'file' => 'ndla_grep_deling.form.inc',
  );
  $items['ndla_grep/ajax/remove'] = array(
    'title' => 'NDLA Grep: Remove',
    'page callback' => 'ndla_grep_deling_ajax_remove',
    'access arguments' => array('access content'),
    'file' => 'ndla_grep_deling.form.inc',
  );
  $items['ndla_grep/lookup/%'] = array(
    'title' => 'NDLA Grep: Course lookup',
    'page callback' => 'ndla_grep_deling_course_lookup',
    'page arguments' => array(2),
    'access arguments' => array('access content'),
  );
  return $items;
}

/**
 * Implements hook_theme().
 */
function ndla_grep_deling_theme($existing, $type, $theme, $path) {
  $theme = array();
  $theme['ndla_grep_deling_aim_select_container'] = array(
    'variables' => array(
      'menu' => NULL,
    ),
    'template' => '/templates/ndla_grep_deling.aim_select_container',
  );
  $theme['ndla_grep_deling_aim_select_list'] = array(
    'variables' => array(
      'items' => NULL,
      'callback' => NULL,
      'target' => 'left',
      'hidden' => FALSE,
      'tree' => array(),
    ),
    'template' => '/templates/ndla_grep_deling.aim_select_list',
  );
  $theme['ndla_grep_deling_aim_select_boxes'] = array(
    'variables' => array(
      'data' => NULL,
      'tree' => array(),
      'selected' => array(),
    ),
    'template' => '/templates/ndla_grep_deling.aim_select_boxes',
  );
  $theme['ndla_grep_deling_aim_selected_container'] = array(
    'variables' => array(
      'items' => NULL,
      'id_list' => array(),
    ),
    'template' => '/templates/ndla_grep_deling.aim_selected_container',
  );
  $theme['ndla_grep_deling_aim_selected_list'] = array(
    'variables' => array(
      'items' => NULL,
    ),
    'template' => '/templates/ndla_grep_deling.aim_selected_list',
  );
  return $theme;
}

function ndla_grep_deling_course_lookup($id) {
  global $base_path;
  $name = NdlaGrepDelingClient::get_curriculum_name($id);
  if(!empty($name)) {
      header('Location: ' . $base_path . 'search?f[0]=ndla_grep_curriculum_facet:' . urlencode($name));
      die();
  }
  drupal_goto('<front>');
}

function ndla_grep_deling_element_children(&$elements, $sort = FALSE) {

    // Do not attempt to sort elements which have already been sorted.
    $sort = isset($elements['#sorted']) ? !$elements['#sorted'] : $sort;

    // Filter out properties from the element, leaving only children.
    $children = array();
    $sortable = FALSE;
    foreach ($elements as $key => $value) {
        if ($key === '' || $key[0] !== '_') {
            $children[$key] = $value;
            if (is_array($value) && isset($value['#weight'])) {
                $sortable = TRUE;
            }
        }
    }

    // Sort the children if necessary.
    if ($sort && $sortable) {
        uasort($children, 'element_sort');

        // Put the sorted children back into $elements in the correct order, to
        // preserve sorting if the same element is passed through
        // element_children() twice.
        foreach ($children as $key => $child) {
            unset($elements[$key]);
            $elements[$key] = $child;
        }
        $elements['#sorted'] = TRUE;
    }
    return array_keys($children);
}