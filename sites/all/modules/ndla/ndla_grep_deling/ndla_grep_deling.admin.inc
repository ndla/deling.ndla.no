<?php

function ndla_grep_deling_settings_form() {
  $form = array();

  $form['service'] = array(
    '#type' => 'fieldset',
    '#title' => t('Service'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    'ndla_grep_account' => array(
      '#title' => t('Account'),
      '#type' => 'textfield',
      '#default_value' => variable_get('ndla_grep_account', ''),
      '#description' => t('The name of the user account, i.e. ":fyr" or ":ndla".', array(':fyr' => 'fyr', ':ndla' => 'ndla')),
    ),
    'ndla_grep_version' => array(
      '#title' => t('Version'),
      '#type' => 'textfield',
      '#default_value' => variable_get('ndla_grep_version', ''),
      '#description' => t('The UDIR import version. If empty the latest version will be used.'),
    ),
    'ndla_grep_host' => array(
      '#title' => t('Host'),
      '#type' => 'textfield',
      '#default_value' => variable_get('ndla_grep_host', ''),
      '#description' => t('The GREP server host name, i.e. ":url".', array(':url' => 'mycurriculum.test.ndlap3.seria.net')),
    ),
    'ndla_grep_ssl' => array(
      '#title' => t('Use SSL'),
      '#type' => 'checkbox',
      '#default_value' => variable_get('ndla_grep_ssl', FALSE),
    ),
    'ndla_grep_username' => array(
      '#title' => t('Username'),
      '#type' => 'textfield',
      '#default_value' => variable_get('ndla_grep_username', ''),
      '#description' => t('The server authentication username.'),
    ),
    'ndla_grep_password' => array(
      '#title' => t('Password'),
      '#type' => 'textfield',
      '#default_value' => variable_get('ndla_grep_password', ''),
      '#description' => t('The server authentication password.'),
    ),
    'ndla_grep_psi' => array(
      '#title' => t('Hostname for PSI'),
      '#type' => 'textfield',
      '#default_value' => variable_get('ndla_grep_psi', ''),
      '#description' => t('If not set the current host will be used. End with trailing slash'),
    ),
    'ndla_grep_timeout' => array(
      '#title' => t('Timeout'),
      '#type' => 'textfield',
      '#default_value' => variable_get('ndla_grep_timeout', 30),
    ),
    'ndla_grep_wakeup' => array(
      '#title' => t('Wakeup time'),
      '#type' => 'textfield',
      '#default_value' => variable_get('ndla_grep_wakeup', 5),
      '#description' => t('Graceful wakeup time. In minutes.'),
    ),
  );

  $form['search'] = array(
    '#type' => 'fieldset',
    '#title' => t('Search'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    'ndla_grep_solr' => array(
      '#title' => t('Solr URL'),
      '#type' => 'textfield',
      '#default_value' => variable_get('ndla_grep_solr', ''),
      '#description' => t('The URL to Solr, i.e. ":url".', array(':url' => 'http://solr.test.ndlap3.seria.net/solr/grep'))
    ),
  );

  $form['settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    'ndla_grep_content_types' => array(
      '#title' => t('Enabled content types'),
      '#type' => 'checkboxes',
      '#options' => node_type_get_names(),
      '#default_value' => variable_get('ndla_grep_content_types', array()),
    ),
  );

  return system_settings_form($form);
}