<?php

/**
 * Implements hook_form_alter().
 */
function ndla_grep_deling_form_alter(&$form, &$form_state, $form_id) {
  /* Return if not a node form */
  if(empty($form['#node']->type) || $form_id != $form['#node']->type . "_node_form") {
    return;
  }
  /* Return if content type is not enabled */
  $content_types = array_filter(variable_get('ndla_grep_content_types', array()));
  if(!in_array($form['#node']->type, $content_types)) {
    return;
  }

  $aims = array();
  if(!empty($form['#node']->nid)) {
    $aims = NdlaGrepDelingClient::load_aims($form['#node']);
  }

  $selected = '';
  if(!empty($aims)) {
    $selected = theme('ndla_grep_deling_aim_selected_container', array('items' => $aims));
  }
  $form['ndla_grep'] = array(
    '#tree' => TRUE,
    'ndla_grep_search' => array(
      '#type' => 'textfield',
      '#title' => t('Search for curiculas'),
    ),
    'ndla_grep_select' => array(
      'markup' => array(
        '#prefix' => '<div id="ndla_grep_select">',
        '#suffix' => '</div>',
        '#type' => 'markup',
        '#markup' => '',
      ),
      'hidden' => array(
        '#type' => 'hidden',
        '#markup' => '',
      ),
    ),
    'ndla_grep_selected' => array(
      'markup' => array(
        '#prefix' => '<div id="ndla_grep_selected">',
        '#suffix' => '</div>',
        '#type' => 'markup',
        '#markup' => '',
      ),
      'data' => array(
        '#type' => 'hidden',
        '#default_value' => json_encode($aims),
      ),
      'hidden' => array(
        '#type' => 'hidden',
        '#default_value' => $selected,
      ),
    ),
  );

//  $form['#validate'][] = 'ndla_grep_fyr_validate';

  $form['#after_build'][] = 'ndla_grep_deling_after_build';
  array_unshift($form['actions']['submit']['#submit'], 'ndla_grep_deling_submit');
}

function ndla_grep_deling_after_build($form, &$form_state) {
  drupal_add_css(drupal_get_path('module', 'ndla_grep_deling') . '/css/ndla_grep_deling.form.css', 'header');
  drupal_add_js(drupal_get_path('module', 'ndla_grep_deling') .'/js/ndla_grep_deling.form.js', 'file');
  if(!empty($form['ndla_grep']['ndla_grep_select']['hidden']['#value'])) {
    $form['ndla_grep']['ndla_grep_select']['markup']['#markup'] = $form['ndla_grep']['ndla_grep_select']['hidden']['#value'];
  }
  if(!empty($form['ndla_grep']['ndla_grep_selected']['hidden']['#value'])) {
    $form['ndla_grep']['ndla_grep_selected']['markup']['#markup'] = $form['ndla_grep']['ndla_grep_selected']['hidden']['#value'];
  }
  return $form;
}

//function ndla_grep_fyr_validate($form, &$form_state) {
//  foreach(array(/*'courses' => 'courses',*/ 'common_courses' => 'common courses') as $key => $name) {
//    $aims = json_decode($form_state['values']['field_' . $key]['ndla_grep_' . $key . '_selected']['aims']);
//    if(sizeof($aims) < 1) {
//      form_set_error('field_' . $key, t('You need to choose at least 1 competance aims for !type.', array('!type' => $name)));
//    }
//  }
//}

function ndla_grep_deling_submit($form, &$form_state) {
  $form_state['node']->ndla_grep_data = $form_state['values']['ndla_grep']['ndla_grep_selected']['data'];
}

function ndla_grep_deling_ajax_search($string) {
  $curricula = NdlaGrepSolrClient::search($string);
  if(!empty($curricula)) {
    $menu = theme('ndla_grep_deling_aim_select_list', array('items' => $curricula, 'callback' => url('ndla_grep/ajax/curriculum')));
    print theme('ndla_grep_deling_aim_select_container', array('menu' => $menu));
  } else {
    print t('No curriculas with that name found.');
  }
  
}

function ndla_grep_deling_ajax_curriculum() {
  $parameters = drupal_get_query_parameters();
  $items = NdlaGrepDelingClient::get_curriculum($parameters['endpoint']);
  print theme('ndla_grep_deling_aim_select_list', array('items' => $items, 'callback' => url('ndla_grep/ajax/set'), 'target' => 'right', 'tree' => $parameters['tree']));
}

function ndla_grep_deling_ajax_set() {
  $parameters = drupal_get_query_parameters();
  $stored = json_decode($parameters['stored'], TRUE);
  $tree = $parameters['tree'];
  
  $selected = array();
  if(!empty($stored[$tree[0]['_id']][$tree[1]['_id']])) {
    $selected = $stored[$tree[0]['_id']][$tree[1]['_id']];
  }
  $items = NdlaGrepDelingClient::get_competence_aim_set($parameters['endpoint']);  
  print theme('ndla_grep_deling_aim_select_boxes', array('data' => $items, 'tree' => $parameters['tree'], 'selected' => $selected));
}

function ndla_grep_deling_ajax_store() {
  $parameters = drupal_get_query_parameters();
  $tree = $parameters['tree'];
  $stored = json_decode($parameters['stored'], TRUE);
  $bool = ndla_grep_deling_recursive_is_set($tree, $stored);
  if(!$bool) {
    $stored = ndla_grep_deling_recursive_set($tree, $stored);
  } else {
    $stored = ndla_grep_deling_recursive_delete($tree, $stored);
  }
  $html = '';
  if(!empty($stored)) {
    $html = theme('ndla_grep_deling_aim_selected_container', array('items' => $stored));
  }
  echo json_encode(array('json' => json_encode($stored), 'html' => $html));
}

function ndla_grep_deling_recursive_is_set($list, $array) {
  if(empty($list)) {
    return TRUE;
  }
  $item = array_shift($list);
  $id = $item['_id'];
  if(!array_key_exists($item['_id'], $array)) {
    return FALSE;
  }
  return ndla_grep_deling_recursive_is_set($list, $array[$id]);
}

function ndla_grep_deling_recursive_set($list, $array) {
  $item = array_shift($list);
  $id = $item['_id'];
  if(!array_key_exists($id, $array)) {
    $array[$id] = $item;
  }
  if(!empty($list)) {
    $array[$id] = ndla_grep_deling_recursive_set($list, $array[$id]);
  }
  return $array;
}

function ndla_grep_deling_recursive_delete($list, $array) {
  $item = array_shift($list);
  $id = $item['_id'];
  if(empty($list)) {
    unset($array[$id]);
    return $array;
  } else {
    $array[$id] = ndla_grep_deling_recursive_delete($list, $array[$id]);
    if(sizeof(ndla_grep_deling_element_children($array[$id])) == 0) {
      unset($array[$id]);
    }
  }
  return $array;
}

function ndla_grep_deling_recursive_get_aims($items) {
  $aims = array();
  $has_children = FALSE;
  foreach($items as $key => $value) {
    if(!in_array($key, array('_name', '_id'))) {
      $aims = array_merge($aims, ndla_grep_deling_recursive_get_aims($value));
      $has_children = TRUE;
    }
  }
  if(!$has_children && !empty($items['_id'])) {
    $aims[] = $items['_id'];
  }
  return $aims;
}

/**
 * Implements hook_field_extra_fields().
 */
function ndla_grep_deling_field_extra_fields() {
  $extra = array();
  $content_types = array_filter(variable_get('ndla_grep_content_types', array()));
  foreach($content_types as $type) {
    $extra['node'][$type] = array(
      'form' => array(
        'ndla_grep' => array(
          'label' => t('Competence aims'),
          'description' => t('Competence aim selector'),
          'weight' => 0,
        ),
      ),
    );
  }
  return $extra;
}

function ndla_grep_deling_ajax_remove() {
  $parameters = drupal_get_query_parameters();
  $id_list = $parameters['id_list'];
  $stored = json_decode($parameters['stored'], TRUE);
  $stored = ndla_grep_deling_recursive_remove($id_list, $stored);
  $html = '';
  if(!empty($stored)) {
    $html = theme('ndla_grep_deling_aim_selected_container', array('items' => $stored));
  }
  echo json_encode(array('json' => json_encode($stored), 'html' => $html));
}

function ndla_grep_deling_recursive_remove($list, $array) {
  $item = array_shift($list);
  $id = $item;
  if(empty($list)) {
    unset($array[$id]);
    return $array;
  } else {
    $array[$id] = ndla_grep_deling_recursive_remove($list, $array[$id]);
    if(sizeof(ndla_grep_deling_element_children($array[$id])) == 0) {
      unset($array[$id]);
    }
  }
  return $array;
}