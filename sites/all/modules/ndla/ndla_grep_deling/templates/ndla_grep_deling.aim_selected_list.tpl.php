<?php
if(empty($items['_name'])) {
  foreach(array_intersect_key($items, array_flip(ndla_grep_deling_element_children($items))) as $item) {
    $temp_list = $id_list;
    $temp_list[] = $item['_id'];
    print theme('ndla_grep_deling_aim_selected_list', array('items' => $item, 'id_list' => $temp_list));
  }
} else { ?>
<ul>
  <li>
    <span>
      <?php
        print $items['_name'];
        $children = ndla_grep_deling_element_children($items);
        if(empty($children)):
      ?>
        <i class="fa fa-times aim-list-remove" data-tree='<?php echo(json_encode($id_list)); ?>'></i>
      <?php endif; ?>
    </span>
    <?php
      foreach(array_intersect_key($items, array_flip(ndla_grep_deling_element_children($items))) as $item) {
        $temp_list = $id_list;
        $temp_list[] = $item['_id'];
        print theme('ndla_grep_deling_aim_selected_list', array('items' => $item, 'id_list' => $temp_list));
      }
    ?>
  </li>
</ul>
<?php } ?>