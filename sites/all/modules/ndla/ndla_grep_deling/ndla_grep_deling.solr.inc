<?php


/**
 * Implements hook_entity_property_info_alter().
 */
function ndla_grep_deling_entity_property_info_alter(&$info) {
  $info['node']['properties']['ndla_grep_competence_aims'] = array(
    'type' => 'list<text>',
    'label' => t('Competance aims'),
    'sanitized' => TRUE,
    'getter callback' => 'ndla_grep_deling_search_api_property_comp_aim_getter_callback',
  );
  
  $info['node']['properties']['ndla_grep_competence_aims_id'] = array(
    'type' => 'list<string>',
    'label' => t('Competance aims id'),
    'sanitized' => TRUE,
    'getter callback' => 'ndla_grep_deling_search_api_property_comp_aim_id_getter_callback',
  );
  
  $info['node']['properties']['ndla_grep_competence_aims_facet'] = array(
    'type' => 'list<string>',
    'label' => t('Competance aims facet'),
    'sanitized' => TRUE,
    'getter callback' => 'ndla_grep_deling_search_api_property_comp_aim_getter_callback',
  );
  
  $info['node']['properties']['ndla_grep_curriculum'] = array(
    'type' => 'list<text>',
    'label' => t('Curriculum'),
    'sanitized' => TRUE,
    'getter callback' => 'ndla_grep_deling_search_api_property_curricula_getter_callback',
  );
  
  $info['node']['properties']['ndla_grep_curriculum_facet'] = array(
    'type' => 'list<string>',
    'label' => t('Curriculum facet'),
    'sanitized' => TRUE,
    'getter callback' => 'ndla_grep_deling_search_api_property_curricula_getter_callback',
  );
}

/**
 * Getter callback for created_week_day property.
 */
function ndla_grep_deling_search_api_property_comp_aim_getter_callback($item) {
  if(arg(0) == 'search') return;
  $content_types = array_filter(variable_get('ndla_grep_content_types', array()));
  if(in_array($item->type, $content_types)) {
    module_load_include('php', 'ndla_grep_deling', 'NdlaGrepDelingClient');
    $data = array();
    $aims = NdlaGrepDelingClient::load_aims_with_names($item, TRUE);
    foreach($aims as $aim) {
      foreach($aim as $name) {
        $data[] = $name;
      }
    }
    return $data;
  }
}

function ndla_grep_deling_search_api_property_comp_aim_id_getter_callback($item) {
  if(arg(0) == 'search') return;
  $content_types = array_filter(variable_get('ndla_grep_content_types', array()));
  if(in_array($item->type, $content_types)) {
    module_load_include('php', 'ndla_grep_deling', 'NdlaGrepDelingClient');
    return array_keys(NdlaGrepDelingClient::load_aims_with_names($item, TRUE));
  }
}

/**
 * Getter callback for created_week_day property.
 */
function ndla_grep_deling_search_api_property_curricula_getter_callback($item) {
  if(arg(0) == 'search') return;
  $content_types = array_filter(variable_get('ndla_grep_content_types', array()));
  if(in_array($item->type, $content_types)) {
    return NdlaGrepDelingClient::load_curricula_with_names($item);
  }
}

/**
 * Allows for alterations to the facet definitions.
 *
 * @param array &$facet_info
 *   The return values of hook_facetapi_facet_info() implementations.
 * @param array $searcher_info
 *   The definition of the searcher that facets are being collected for.
 *
 * @see hook_facetapi_facet_info()
 */
function ndla_grep_deling_facetapi_facet_info_alter(array &$facet_info, array $searcher_info) {
  if(!empty($facet_info['ndla_grep_competence_aims_id'])) {
    $facet_info['ndla_grep_competence_aims_id']['map options']['value callback'] = 'ndla_grep_deling_get_aim_name';
  }
}

function ndla_grep_deling_get_aim_name($ids, $facet_info) {
  $map = array();
  foreach($ids as $id) {
    $map[$id] = NdlaGrepDelingClient::getAimNames($id);
  }

  return $map;
}

/**********************************************************************
 * Delete everything under this line after the filter has been disabled.
 **********************************************************************/

/**
 * Implements hook_search_api_alter_callback_info()
 */
function ndla_grep_deling_search_api_alter_callback_info() {
  $callbacks['ndla_grep_deling_competence_aims_alter'] = array(
    'name' => t('NDLA Grep Competence aims - disable me'),
    'description' => t('Adds competence aim information to the index.'),
    'class' => 'NdlaGrepDelingCompetenceAimsAlterSettings',
    'weight' => 100,
  );
  return $callbacks;
}

function ndla_grep_deling_search_api_solr_query_alter($call_args, $query) {
  $call_args['params']['qf'][] = 'sm_ndla_grep_competence_aims_id^1';
}

/**
 * @file
 * Search API data alteration callback that adds the workflow state info to the entity.
 */
class NdlaGrepDelingCompetenceAimsAlterSettings extends SearchApiAbstractAlterCallback {

  public function alterItems(array &$items) {
    $content_types = array_filter(variable_get('ndla_grep_content_types', array()));
    foreach ($items as $id => &$item) {
      if(in_array($item->type, $content_types)) {
        $item->ndla_grep_competence_aims = NdlaGrepDelingClient::load_aims_with_names($item);
      }
    }
  }

  public function supportsIndex(SearchApiIndex $index) {
    return $index->getEntityType() == 'node';
  }

  public function propertyInfo() {
    return array(
      'ndla_grep_competence_aims' => array(
        'label' => t('Competence aims'),
        'description' => t('The competence aims of the node.'),
        'type' => 'list<string>',
      ),
    );
  }

}