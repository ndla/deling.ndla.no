<?php

/*
 * Example: Rename to custom.php to enable.
 */


class SeriaAuthCustomCode
{
	/*
	 * Special configuration (overrides default):
	 */
	public static $autmaticUnsafeEmailMatch = false/*default is false*/;

	/**
	 *
	 * Callback when a user has been successfully logged in by this module.
	 * @param $account object The drupal account
	 * @param $firstLogin boolean Is this the first login?
	 */
	public function loggedIn($account, $firstLogin)
	{
	}
}