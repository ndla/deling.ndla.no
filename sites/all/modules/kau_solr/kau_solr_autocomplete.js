
/**
 * Prevents the form from submitting if the suggestions popup is open
 * and closes the suggestions popup when doing so.
 */

Drupal.autocompleteSubmit = function () {
  return jQuery('#autocomplete').each(function () {
    this.owner.hidePopup();
  });
};

Drupal.jsAC.prototype.select = function (node) {
  this.input.value = jQuery(node).data('autocompleteValue');
  if(this.input.id == 'edit-keys' || this.input.id == 'edit-keys--2') {
    jQuery(this.input).parents('form').submit();
  }
};