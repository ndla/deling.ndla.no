<?php
/**
 * Make Drupal boot fast. The autocomplete takes around 450ms without this, with this around 90ms.
 */
 
chdir('../../../../');
define('DRUPAL_ROOT', getcwd());
include_once "includes/bootstrap.inc";
include_once "includes/common.inc";
/*
$bootstraps = array(
  DRUPAL_BOOTSTRAP_CONFIGURATION,
//  DRUPAL_BOOTSTRAP_PAGE_CACHE,
//  DRUPAL_BOOTSTRAP_DATABASE,
  DRUPAL_BOOTSTRAP_VARIABLES,
//  DRUPAL_BOOTSTRAP_SESSION,
//  DRUPAL_BOOTSTRAP_PAGE_HEADER,
  DRUPAL_BOOTSTRAP_LANGUAGE,
  
);
foreach($bootstraps as $bootstrap) {
  drupal_bootstrap($bootstrap);
}

//Modules to load
$modules = array(
  'rules',
  'entity',
  'search_api',
  'search_api_solr',
  'kau_solr',
  'kau_solr_autocomplete'
);

foreach($modules as $module) {
  drupal_load('module', $module);
}*/

drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

$debug = !empty($_REQUEST['debug']);
$index = arg(1);
$word = arg(2);

if($debug) {
  echo "Index: $index<br />";
  echo "Word: $word<br />";
}

$e = kau_solr_autocomplete_get_results($index, $word, $debug);
echo "<pre>" . print_r($e, true) . "</pre>";
?>